"""CLI tool for pyAMNESIA."""
import argparse
from pyamnesia.cli.pyamnesia_analysis_skeleton import PyamnesiaAnalysisSkeleton
from pyamnesia.cli.pyamnesia_analysis_clustering import PyamnesiaAnalysisClustering
from pyamnesia.cli.pyamnesia_analysis_factorization import PyamnesiaAnalysisFactorization


def parse_args(args):
    parser = argparse.ArgumentParser(description="CLI tool for pyAMNESIA")
    parser.add_argument("-m", "--method", choices=["skeleton", "clustering", "factorization"], required=False, help="Which method to use", type=str)

    return parser.parse_args(args)


def main(args):
    args = parse_args(args)
    method = args.method
    try:
        while method not in ["skeleton", "clustering", "factorization"]:
            method = input("[INPUT] method? (skeleton, clustering, factorization)\n>> ")
    except (EOFError, KeyboardInterrupt):
        print("exit")  # to avoid ugly output
        sys.exit(0)

    if method == "skeleton":
        analysis = PyamnesiaAnalysisSkeleton()
    elif method == "clustering":
        analysis = PyamnesiaAnalysisClustering()
    elif method == "factorization":
        analysis = PyamnesiaAnalysisFactorization()
    else:
        raise ValueError(f"{method} has not been implemented. argparse choices for --method may be broken.")
    analysis.run_analysis()


if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
