"""Skeletonization analysis for pyAMNESIA CLI."""
import os
from time import time
from pyamnesia.cli.pyamnesia_analysis import PyamnesiaAnalysis
from pyamnesia.signal_processing.sequence_processing import denoise_sequence
from pyamnesia.skeleton.skeletonize_sequence import skeletonize_sequence


class PyamnesiaAnalysisSkeleton(PyamnesiaAnalysis):

    """Skeletonization analysis for pyAMNESIA CLI.

    Attributes
    ----------
    analysis_start_time : time.time()
        Starting time of the analysis. Useful for the folder name.
    config : dict
        Full config dictionary.
    data_to_analyse : list
        List of ``.tif`` sequences that are going to be analyzed.
    logger : Logger
        Logger for printing both in terminal and in log.txt file.
    name : str
        Small string that describes the analysis, for display.
    results_path : str
        Where the results are stored.
    """

    def __init__(self):
        """Class initialization."""
        super().__init__("Skeleton")

    def run_analysis(self, input_path=None):
        """Run the skeletonization analysis.

        Parameters
        ----------
        input_path : str, list, tuple, optional
            Contains the image paths to analyze. If ``None``, opens a file
            dialog box.

        Raises
        ------
        ValueError
            If **input_path** is not a supported path input type.

        Notes
        -----
        If you want to process several files, you may want to give their name
        as a ``list`` to **run_analysis**. It will create one session for the
        whole list instead of one for each file.
        """
        super().run_analysis(input_path)
        config_misc = self.config['misc']
        config_denoising = self.config['denoising']
        config_skeleton = self.config['skeleton']

        verbose = True

        # start runs
        n_sessions = len(self.data_to_analyse)
        plural = "s" if n_sessions >= 2 else ""
        print(f"{n_sessions} session{plural}")

        for session_path, session_data in zip(self._path_to_data_to_analyse, self.data_to_analyse):
            session_identifier = os.path.splitext(os.path.basename(os.path.realpath(session_path)))[0]
            print(f"-------------- {session_identifier} -------------- ")

            config_misc['results_path'] = os.path.join(self.results_path, session_identifier)

            # denoise sequence
            denoised_tif = denoise_sequence(session_data, config_denoising, config_misc)
            # skeletonize
            projected_coordinates, projected_traces, summary_image, projected_mask = skeletonize_sequence(denoised_tif, config_skeleton, config_misc)

            if verbose:
                print(" ")
