"""An abstract class that should be inherited in order to create a specific
analyse for the pyAMNESIA CLI."""
import os
import sys
import yaml
import json
import tkinter as tk
from time import time
from datetime import datetime
from abc import ABC, abstractmethod
from pyamnesia.utils import read_tif, Logger, NoInputError, NoDirectoryError


class PyamnesiaAnalysis(ABC):

    """An abstract class that should be inherited in order to create a specific
    analyse for the pyAMNESIA CLI."""

    def __init__(self, name):
        """Class initialization.

        Parameters
        ----------
        name : str
            Small string that describes the analysis, for display.
        """
        # name
        self.name = name
        print(f"############### {name.upper()} ###############")
        # input
        self._path_to_data_to_analyse = None
        self.data_to_analyse = []
        # config
        self.results_path = None
        self.config = None
        self._build_config()
        # logger
        self.logger = Logger(os.path.join(self.results_path, "log.txt"), "w+")
        sys.stdout = self.logger

    def __str__(self):
        desc = f"Name: {self.name}\n" + \
            f"Results path: {self.results_path}\n" + \
            f"Config: {self.config}\n"
        # f"Config: {json.dumps(self.config, indent=4)}\n"
        return desc

    def _build_config(self):
        """To make the __init__ method shorter."""
        # open config
        path_to_config = os.path.join(os.path.abspath(os.path.dirname(__file__)), '../config.yaml')
        with open(path_to_config) as infile:
            config = yaml.load(infile, Loader=yaml.FullLoader)
        # update results path
        results_path = config['misc']['results_path']
        time_str = datetime.now().strftime("%Y_%m_%d.%H-%M-%S")
        base_name = self.name + f"_{time_str}"
        yaml_file_name = base_name + ".yaml"
        self.results_path = os.path.join(results_path, base_name)
        config['misc']['results_path'] = self.results_path
        os.mkdir(os.path.realpath(self.results_path))
        self.config = config
        # save config
        with open(os.path.join(self.results_path, yaml_file_name), 'w') as outfile:
            yaml.dump(self.config, outfile, default_flow_style=False)

        # start analysis timer
        self.analysis_start_time = time()

    def check_tif_data(self):
        """Check if the elements of `self._path_to_data_to_analyse` have the
        right extensions.

        Raises
        ------
        TypeError
            If one element of `self._path_to_data_to_analyse` doesn't have the
            right extension.
        """
        for path in self._path_to_data_to_analyse:
            extension = os.path.splitext(path)[1]
            if not extension in [".tif", ".tiff"]:
                raise TypeError(f"Bad input type ({extension}) for {os.path.basename(os.path.realpath(path))}.")

    @abstractmethod
    def run_analysis(self, input_path=None):
        """Run an analysis.

        Parameters
        ----------
        input_path : str, list, tuple, optional
            Contains the image paths to analyze. If ``None``, opens a file
            dialog box.

        Raises
        ------
        ValueError
            If **input_path** is not a supported path input type.

        Notes
        -----
        If you want to process several files, you may want to give their name
        as a ``list`` to **run_analysis**. It will create one session for the
        whole list instead of one for each file.
        """
        if input_path is None:  # i.e. we want to open a file dialog
            input_path = self.open_dialog_tif_input()  # will return a list
        # now we have either a str or a list
        if isinstance(input_path, str):
            self._path_to_data_to_analyse = [input_path]
        elif isinstance(input_path, list):
            self._path_to_data_to_analyse = input_path
        else:
            raise ValueError(f"{type(input_path)} is not a supported path input type.")
        self.check_tif_data()

        # load data
        assert isinstance(self._path_to_data_to_analyse, list)
        for path in self._path_to_data_to_analyse:
            self.data_to_analyse.append(self.get_tif_data_from_path(path))

    def get_tif_data_from_path(self, path):
        """Return ``.tif`` from path.

        Parameters
        ----------
        path :
            Path of the ``.tif`` sequence.

        Returns
        -------
        tif_sequence : np.array
            ``.tif`` sequence.
        """
        return read_tif(path, self.config['read_tif'])

    def open_dialog_tif_input(self):
        """Open a dialog box and return the selected ``.tif``s.

        Returns
        -------
        files : list
            List of ``.tif``s.

        Raises
        ------
        NoInputError
            If no ``.tif`` is specified.
        """
        title = "Choose input TIF file(s)"
        filetypes = [("TIF images", "*.tif;*.tiff")]
        filetypes_string = "\n".join([f"  {type_tuple[0]}: {type_tuple[1]}" for type_tuple in filetypes])
        print(f"[INPUT] {title}\n{filetypes_string}")
        root = tk.Tk()
        root.withdraw()  # don't show root
        files = tk.filedialog.askopenfilenames(title=title, filetypes=filetypes)
        root.destroy()
        if files == '':  # if no file is selected
            raise NoInputError()
        return [file for file in files]   # from tuple to list

    def open_dialog_dir(self):
        """Open a dialog box to choose the skeletonization results folder and
        return the selected directory.

        Returns
        -------
        directory : str
            Skeletonization results directory.

        Raises
        ------
        NoDirectoryError
            If no folder is specified.
        """
        title = "Choose skeleton results directory"
        print(f"[INPUT] {title}")
        root = tk.Tk()
        root.withdraw()  # don't show root
        directory = tk.filedialog.askdirectory(title=title)
        root.destroy()
        if directory == '':
            raise NoDirectoryError()
        return directory
