"""Factorization analysis for pyAMNESIA CLI."""
import os
import numpy as np
from time import time
from pyamnesia.cli.pyamnesia_analysis import PyamnesiaAnalysis
from pyamnesia.correlation.factorization.factorization import factorization


class PyamnesiaAnalysisFactorization(PyamnesiaAnalysis):

    """Factorization analysis for pyAMNESIA CLI.

    Attributes
    ----------
    analysis_start_time : time.time()
        Starting time of the analysis. Useful for the folder name.
    config : dict
        Full config dictionary.
    data_to_analyse : list
        List of ``.tif`` sequences that are going to be analyzed.
    logger : Logger
        Logger for printing both in terminal and in log.txt file.
    name : str
        Small string that describes the analysis, for display.
    results_path : str
        Where the results are stored.
    """

    def __init__(self):
        """Class initialization."""
        super().__init__("Factorization")

    def run_analysis(self, input_path=None):
        """Run the factorization analysis.

        Parameters
        ----------
        input_path : str, list, tuple, optional
            Contains the image paths to analyze. If ``None``, opens a file
            dialog box.

        Raises
        ------
        ValueError
            If **input_path** is not a supported path input type.

        Notes
        -----
        If you want to process several files, you may want to give their name
        as a ``list`` to **run_analysis**. It will create one session for the
        whole list instead of one for each file.
        """
        super().run_analysis(input_path)
        config_misc = self.config['misc']
        config_factorization = self.config['factorization']

        verbose = True

        # start runs
        n_sessions = len(self.data_to_analyse)
        plural = "s" if n_sessions >= 2 else ""
        print(f"{n_sessions} session{plural}")
        path_to_skeleton_data = self.open_dialog_dir()

        for session_path, session_data in zip(self._path_to_data_to_analyse, self.data_to_analyse):
            session_identifier = os.path.splitext(os.path.basename(os.path.realpath(session_path)))[0]
            print(f"-------------- {session_identifier} -------------- ")

            config_misc['results_path'] = os.path.join(self.results_path, session_identifier)

            # get skeleton results
            path_to_skeleton_data_session = os.path.join(path_to_skeleton_data, session_identifier)

            # check if files are present
            file_folders = ['sequence_projection', 'sequence_projection']
            file_names = ['projected_traces.npy', 'projected_coordinates.npy']
            files_are_present = True
            for file_folder, file_name in zip(file_folders, file_names):
                if not os.path.isfile(os.path.join(path_to_skeleton_data_session, file_folder, file_name)):
                    print(f"Unable to find a skeleton result ({file_name}).")
                    files_are_present = False
                    break
            if not files_are_present:
                print(f"Skipping {session_identifier}...\n")
                continue

            # load data
            projected_traces = np.load(os.path.join(path_to_skeleton_data_session, 'sequence_projection', 'projected_traces.npy'))
            projected_coordinates = np.load(os.path.join(path_to_skeleton_data_session, 'sequence_projection', 'projected_coordinates.npy'), allow_pickle=True)

            # factorization
            components = factorization(
                tif_sequence=session_data,
                traces=projected_traces,
                projected_coordinates=projected_coordinates,
                config_factorization=config_factorization,
                config_misc=config_misc,
            )

            if verbose:
                print(" ")
