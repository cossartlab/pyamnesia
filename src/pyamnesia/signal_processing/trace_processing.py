"""Perform trace processing."""
import numpy as np
import pandas as pd
from scipy.stats import zscore


def smooth_trace(trace, window_len=11, window='hanning'):
    r"""Smooth the data using a window of the requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    Parameters
    ----------
    trace : np.array
        The input signal.
    window_len : int
        The dimension of the smoothing window; should be an odd integer.
    window : str
        The type of window: either ``'flat'``, ``'hanning'``, ``'hamming'``,
        ``'bartlett'`` or ``'blackman'``. A ``'flat'`` window will produce a
        moving average smoothing.

    Returns
    -------
    smoothed_trace : np.array
        The smoothed trace.

    Raises
    ------
    ValueError

    See also
    --------
    np.hanning, np.hamming, np.bartlett, np.blackman, np.convolve,
    scipy.signal.lfilter

    Notes
    -----
    - The window parameter could be the window itself if an `array` instead of
      a `str`
    - ``length(output) != length(input)``. To correct this, use
      ``return y[(window_len/2-1):-(window_len/2)]`` instead of just ``y``.

    References
    ----------
    - https://github.com/scipy/scipy-cookbook/blob/master/ipython/SignalSmooth.ipynb

    Examples
    --------
    >>> t = linspace(-2, 2, 0.1)
    >>> x = sin(t) + randn(len(t)) * 0.1
    >>> y = smooth(x)
    """
    if trace.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if trace.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")

    if window_len < 3:
        return trace

    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")

    s = np.r_[trace[window_len - 1:0:-1], trace, trace[-2:-window_len - 1:-1]]
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window + '(window_len)')

    y = np.convolve(w / w.sum(), s, mode='valid')
    return y[window_len // 2:-window_len // 2 + 1]


def smooth_traces(traces, window_len=11, window='hanning'):
    """Smooth all the traces.

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    window_len : int, optional
        The dimension of the smoothing window. Should be an odd integer.
    window : str, optional
        The type of window: either ``'flat'``, ``'hanning'``, ``'hamming'``,
        ``'bartlett'`` or ``'blackman'``. A ``'flat'`` window will produce a
        moving average smoothing.

    Returns
    -------
    smoothed_traces : np.array
        The smoothed traces.
    """
    smoothed_traces = []
    for trace in traces:
        smoothed_trace = smooth_trace(trace, window_len=window_len, window=window)
        smoothed_traces.append(smoothed_trace)
    smoothed_traces = np.array(smoothed_traces)
    return smoothed_traces


def normalize_traces(traces, normalization_method='z-score'):
    r"""Normalize the traces.

    Three methods are implemented to normalize a trace :math:`T`:

        - ``'null'``: no normalization
        - ``'mean-substraction'``: :math:`T = T - \overline T`
        - ``'z-score'``: :math:`T = (T - \overline T) / \sigma(T)`

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    normalization_method : str, optional
        The method for normalizing the traces. It can be one of ``'null'``,
        ``'mean-substraction'`` and ``'z-score'``.

    Returns
    -------
    normalized_traces : np.array
        The array of normalized traces.

    Raises
    ------
    NotImplementedError
        When a method name isn't related to any implemented method.
    """
    if normalization_method is None:
        normalized_traces = traces
        return normalized_traces
    elif normalization_method == 'z-score':
        normalized_traces = zscore(traces, axis=1)
        return normalized_traces
    elif normalization_method == 'mean-substraction':
        normalized_traces = traces - traces.mean(axis=1)[:, np.newaxis]
        return normalized_traces
    else:
        raise NotImplementedError(f"The method {normalization_method} has not been implemented")


def rank_trace(trace):
    """Rank a trace.

    Each value taken by the trace is attributed its position in the sorted
    trace array.

    Parameters
    ----------
    trace : np.array
        Trace of the entity (pixel, branch...) to analyze.

    Returns
    -------
    ranked_trace : np.array
        The ranked trace.
    """
    df_trace = pd.DataFrame(trace)
    ranked_trace = np.array(df_trace.rank()).flatten()
    return ranked_trace


def rank_traces(traces):
    """Rank all the traces.

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.

    Returns
    -------
    ranked_trace : np.array
        The ranked traces.
    """
    ranked_traces = []
    for trace in traces:
        ranked_trace = rank_trace(trace)
        ranked_traces.append(ranked_trace)
    ranked_traces = np.array(ranked_traces)
    return ranked_traces
