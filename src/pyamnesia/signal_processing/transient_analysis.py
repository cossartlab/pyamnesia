"""Perform transients analysis."""
import numpy as np
from scipy.signal import find_peaks, peak_prominences
from scipy.stats import norm


def get_transients_frames(trace, peak_properties={}):
    """Get the frames of the transients.

    What we call a transient is defined by the **peak_properties** dictionary.
    For more details, please refer to the ``scipy.signal.find_peaks`` function.

    Parameters
    ----------
    trace : np.array
        Trace of the entity (pixel, branch...) to analyze.
    peak_properties : dict, optional
        Dictionary to parametrize the traces peaks detection.
        Contains **height**, **threshold**, **distance**, **prominence** and
        **width**.

    Returns
    -------
    transients_frames : list
        List of the indices of the frames where the entity has a transient.

    References
    ----------
    - ``scipy.signal.find_peaks``
    """
    height = peak_properties['height']
    threshold = peak_properties['threshold']
    distance = peak_properties['distance']
    prominence = peak_properties['prominence']
    width = peak_properties['width']

    # Get the indices of trace peaks, according to the peak properties
    transients_frames, _ = find_peaks(trace,
                                      height=height,
                                      threshold=threshold,
                                      distance=distance,
                                      prominence=prominence,
                                      width=width)
    return transients_frames


def get_transients_trace(trace, transients_frames, transients_values='prominence', binning_radius=1):
    """Turn the trace into a **transient trace**.

    The array is set to ``0`` everywhere, except for the frames surrounding the
    transients. To these ones is attributed a value - that depends on the
    **transients_values** parameter -, spread to the neighbouring frames `via`
    convolution with a gaussian smoothing of standard deviation
    **binning_radius**.

    Parameters
    ----------
    trace : np.array
        Trace of the entity (pixel, branch...) to analyze.
    transients_frames : list
        List of the indices of the frames where the entity has a transient.
    transients_values : str
        The values of transients used in the `trace to transient` transform.
        Can be one of ``'constant'``, ``'height'`` and ``'prominence'``.
    binning_radius : int
        The standard deviation used during transients convolution.

    Returns
    -------
    transients_trace : np.array
        The trace transformed to an array of transients. Depends on the
        **transients_values** and **binning_radius** parameters.
    """
    transients_trace = np.zeros_like(trace)

    if transients_values == 'constant':
        # Set each transient to 1
        transients_trace[transients_frames] = 1

    elif transients_values == 'height':
        # Set each transient to its value
        # We make the trace positive so that the transients are positives
        trace_pos = trace + abs(np.min(trace))
        transients_trace[transients_frames] = trace_pos[transients_frames]

    elif transients_values == 'prominence':
        # Set each transient to its peak prominence
        peaks_prominences = peak_prominences(trace, transients_frames)[0]
        transients_trace[transients_frames] = peaks_prominences

    else:
        raise NotImplementedError(f"The method {transients_values} hasn't been implemented yet.")

    # Convolve the signal with a gaussian window
    convolution_range = np.arange(-binning_radius, binning_radius + 1)
    convolution_signal = norm.pdf(convolution_range, loc=0, scale=binning_radius)
    transients_trace = np.convolve(transients_trace, convolution_signal, mode='same')

    return transients_trace


def get_transients_traces(traces, peak_properties={}, transients_values='prominence', binning_radius=1):
    """Get the ``'transients_traces'`` of all traces.

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    peak_properties : dict, optional
        Dictionary to parametrize the traces peaks detection.
        Contains **height**, **threshold**, **distance**, **prominence** and
        **width**.
    transients_values : str
        The values of transients used in the `trace to transient` transform.
    binning_radius : int
        The standard deviation used during transients convolution.

    Returns
    -------
    transients_traces : np.array
        The array of the traces transformed to an array of transients.
    """
    transients_traces = []
    for trace in traces:
        transients_frames = get_transients_frames(trace, peak_properties)
        transients_trace = get_transients_trace(trace, transients_frames, transients_values, binning_radius)
        transients_traces.append(transients_trace)

    transients_traces = np.array(transients_traces)
    return transients_traces
