"""Perform sequence preprocessing."""
import numpy as np
from pyamnesia.utils import timed, save_data
from scipy.ndimage import gaussian_filter


@timed
def denoise_sequence(tif_sequence, config_denoising, config_misc):
    """Denoise the sequence using a gaussian smoothing.

    The smoothing can either be :
        - ``'2D'``: each 2D-frame is smoothed separately.
        - ``'3D'``: the movie is considered as a 3D image and smoothed as if.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    config_denoising : dict
        Config dictionary for `denoising`.
        Contains **denoising_method**, which can either be ``'2D'`` or ``'3D'``,
        and **sigma_denoising**, that is the standard deviation of the gaussian
        smoothing.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    denoised_sequence : np.array
        ``.tif`` calcium imaging sequence after being denoised with gaussian
        smoothing.

    Raises
    ------
    NotImplementedError
        When a method name isn't related to any implemented method.
    """
    denoising_method = config_denoising['denoising_method']
    sigma_denoising = config_denoising['sigma_denoising']
    results_path = config_misc['results_path']
    save_denoised_sequence = config_misc['save_denoised_sequence']

    if denoising_method == '2D':
        # Filter each frame
        denoised_sequence = np.array([gaussian_filter(frame, sigma=sigma_denoising) for frame in tif_sequence])

    elif denoising_method == '3D':
        # Filter the whole 3D "image"
        denoised_sequence = gaussian_filter(tif_sequence, sigma=sigma_denoising)

    else:
        raise NotImplementedError(f"The method {denoising_method} hasn't been implemented.")

    # Save denoised sequence in a ``.tif`` format
    if save_denoised_sequence:
        print("[INFO] Saving denoised sequence...")
        save_data(denoised_sequence, 'tif', results_path, "preprocessing", f"denoised_sequence.tif")

    return denoised_sequence
