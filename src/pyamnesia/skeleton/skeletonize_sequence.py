"""Perform sequence skeletonization."""
from pyamnesia.utils import save_data
from pyamnesia.skeleton.modules.skeleton_mask import get_skeleton_mask
from pyamnesia.skeleton.modules.pixel_projection import pixel_projection
from pyamnesia.skeleton.modules.branch_projection import branch_projection
from pyamnesia.skeleton.modules.branch_validation import validate_branches


def skeletonize_sequence(tif_sequence, config_skeleton, config_misc):
    """Perform sequence skeletonization.

    First extract a summary image from the movie.
    Then perform morphological operations to get the skeleton of the summary image.
    Finally project the sequence on the skeleton, either pixel by pixel, or branch by branch, with intensity uniformization.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    config_skeleton : dict
        Config dictionary for `skeleton`.
        Contains the sub config dictionaries  **config_skeleton_mask** ,
        **config_element**, **config_validate_branches** and the parameter
        **projection_method**, that can be either ``'pixel'`` or ``'branch'``.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    projected_coordinates : np.array
        Coordinates of the components of the entities. Used to unravel the
        vectors.
    projected_traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    summary_image : np.array
        Image that summarizes the ``.tif`` sequence, following the
        **summarize_sequence** method.
    skeleton_mask : np.array
        Binary image of the skeleton mask.

    Raises
    ------
    NotImplementedError
        When a method name isn't related to any implemented method.
    """
    projection_method = config_skeleton['projection_method']
    results_path = config_misc['results_path']

    # Get skeleton mask
    summary_image, skeleton_mask = get_skeleton_mask(tif_sequence, config_skeleton['skeleton_mask'], config_misc)

    # Get the components coordinates and traces
    if projection_method == 'pixel':
        projected_coordinates, projected_traces = pixel_projection(tif_sequence, skeleton_mask,
                                                                 config_skeleton['element'],
                                                                 config_misc)

    elif projection_method == 'branch':
        branches_coordinates, branches_traces = branch_projection(tif_sequence, skeleton_mask,
                                                                  config_skeleton['element'],
                                                                  config_misc)

        projected_coordinates, projected_traces = validate_branches(tif_sequence, branches_coordinates,
                                                                  branches_traces,
                                                                  config_skeleton['validate_branches'],
                                                                  config_misc)
    else:
        raise NotImplementedError(f"The method {projection_method} has not been implemented")

    # Save the components coordinates and traces in ``.npy`` format
    print("[INFO] Saving components...")
    save_data(projected_coordinates, 'npy', results_path, "sequence_projection", "projected_coordinates.npy")
    save_data(projected_traces, 'npy', results_path, "sequence_projection", "projected_traces.npy")

    return projected_coordinates, projected_traces, summary_image, skeleton_mask
