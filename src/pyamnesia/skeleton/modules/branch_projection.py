"""Perform intensity projection of the sequence on the skeleton, branch by
branch with intensity uniformization."""
import numpy as np
from skan import csr
from pyamnesia.utils import timed, save_data
from pyamnesia.signal_processing.trace_processing import normalize_traces, smooth_traces


def get_branches_coordinates(skeleton_mask, branch_radius=0):
    """Get the coordinates of the pixels of the branches in the skeleton mask.

    Parameters
    ----------
    skeleton_mask : np.array
        Binary image of the skeleton mask.
    branch_radius : int, optional
        The radius of the branches. If set to ``0``, the branches remain
        one-pixel wide. Otherwise they get dilated.

    Returns
    -------
    branches_coordinates : np.array
        Coordinates of the components of the branches. Used to unravel the
        vectors.
    """
    height, width = skeleton_mask.shape

    # Get skeleton branches with Skan
    skeleton = csr.Skeleton(skeleton_mask)
    path_coordinates = skeleton.path_coordinates
    n_paths = skeleton.n_paths

    branches_coordinates = []
    for path_id in range(n_paths):
        branch_coordinates = path_coordinates(path_id)
        # Convert the branch to the right type
        branch_coordinates = np.rint(branch_coordinates).astype(int)
        # Dilate the branch coordinates
        dilated_branch_coordinates = dilate_branch_coordinates(branch_coordinates, height, width, branch_radius)
        branches_coordinates.append(dilated_branch_coordinates)

    branches_coordinates = np.array(branches_coordinates, dtype=object)
    return branches_coordinates


def dilate_branch_coordinates(branch_coordinates, height, width, branch_radius=0):
    """Dilate the array of coordinates of a branch, with a predefined radius.

    Parameters
    ----------
    branch_coordinates : np.array
        Coordinates of the components of one branch. Used to unravel the
        vectors.
    height : int
        Height of the skeleton mask.
    width : int
        Width of the skeleton mask.
    branch_radius : int, optional
        The radius of the branches. If set to ``0``, the branches remain
        one-pixel wide. Otherwise they get dilated.

    Returns
    -------
    dilated_branch_coordinates : np.array
        Dilated coordinates of the components of one branch. Used to unravel
        the vectors.
    """
    dilated_branch_coordinates = []
    for i, j in branch_coordinates:
        if branch_radius <= min(i, j) and i < height - branch_radius and j < width - branch_radius:
            for SEi in range(-branch_radius, branch_radius + 1):
                for SEj in range(-branch_radius, branch_radius + 1):
                    if [i + SEi, j + SEj] not in dilated_branch_coordinates:
                        dilated_branch_coordinates.append([i + SEi, j + SEj])
        else:
            if [i, j] not in dilated_branch_coordinates:
                dilated_branch_coordinates.append([i, j])

    dilated_branch_coordinates = np.array(dilated_branch_coordinates)
    return dilated_branch_coordinates


def get_branch_dataframe(skeleton_mask):
    """Get a DataFrame containing statistics about the branches, provided by
    the ``Skan`` module.

    Parameters
    ----------
    skeleton_mask : np.array
        Binary image of the skeleton mask.

    Returns
    -------
    branch_dataframe : pandas.DataFrame
        A DataFrame containing statistics about the branches: euclidean length,
        path length, extreme pixels coordinates, etc...

    References
    ----------
    - ``skan``: https://github.com/jni/skan/
    """
    skeleton = csr.Skeleton(skeleton_mask)
    branch_dataframe = csr.summarize(skeleton)
    return branch_dataframe


def get_branches_traces(tif_sequence, branches_coordinates):
    """Get the mean traces of the skeleton branches.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    branches_coordinates : np.array
        Coordinates of the components of the branches. Used to unravel the
        vectors.

    Returns
    -------
    branches_traces : np.array
        Traces of the branches to analyze.
    """
    branches_traces = []
    for branch_coordinates in branches_coordinates:
        # Spatially average the intensities along the branch
        branch_trace = tif_sequence[:, branch_coordinates[:, 0], branch_coordinates[:, 1]].mean(axis=1)
        branches_traces.append(branch_trace)

    branches_traces = np.array(branches_traces)
    return branches_traces


def get_branch_sequence(tif_sequence, branches_traces, branches_coordinates):
    """Get the sequence projected on the skeleton.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    branches_traces : np.array
        Traces of the branches to analyze.
    branches_coordinates : np.array
        Coordinates of the components of the branches. Used to unravel the
        vectors.

    Returns
    -------
    branch_sequence : np.array
        ``.tif`` sequence projected on the skeleton branches. The intensity is
        constant along a branch.
    """
    branch_sequence = np.zeros_like(tif_sequence)
    for branch_id, branch_coordinates in enumerate(branches_coordinates):
        for i, j in branch_coordinates:
            branch_sequence[:, i, j] = branches_traces[branch_id]
    return branch_sequence


@timed
def branch_projection(tif_sequence, skeleton_mask, config_branch_projection, config_misc):
    """Perform intensity projection of the sequence on the skeleton, branch by
    branch, with intensity uniformization.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    skeleton_mask : np.array
        Binary image of the skeleton mask.
    config_branch_projection :
        Config dictionary for `branch_projection`.
        Contains **element_normalization_method**, that can be one of ``'null'``,
        ``'mean-substraction'`` and ``'z-score'``, and
        **element_smoothing_window_len** that is relative to the trace
        smoothing window parameter.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    branches_coordinates : np.array
        Coordinates of the components of the branches. Used to unravel the
        vectors.
    branches_traces : np.array
        Traces of the branches to analyze.
    """
    branch_radius = config_branch_projection['branch_radius']
    normalization_method = config_branch_projection['element_normalization_method']
    smoothing_window_len = config_branch_projection['element_smoothing_window_len']
    results_path = config_misc['results_path']
    save_projected_sequence = config_misc['save_projected_sequence']
    save_branch_dataframe = config_misc['save_branch_dataframe']

    # Get branches coordinates
    branches_coordinates = get_branches_coordinates(skeleton_mask, branch_radius=branch_radius)
    if save_branch_dataframe:
        branch_dataframe = get_branch_dataframe(skeleton_mask)
        print("[INFO] Saving branch DataFrame...")
        save_data(branch_dataframe, 'xlsx', results_path, "sequence_projection", "branch_dataframe.xlsx")

    # Get branches traces
    branches_traces = get_branches_traces(tif_sequence, branches_coordinates)

    # Save projected sequence
    if save_projected_sequence:
        branch_sequence = get_branch_sequence(tif_sequence, branches_traces, branches_coordinates)
        print("[INFO] Saving skeleton sequence (branch)...")
        save_data(branch_sequence, 'tif', results_path, "sequence_projection", "projected_sequence.tif")

    # Smooth traces
    branches_traces = smooth_traces(branches_traces, window_len=smoothing_window_len)

    # Normalize traces
    branches_traces = normalize_traces(branches_traces, normalization_method=normalization_method)

    return branches_coordinates, branches_traces
