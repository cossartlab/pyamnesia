"""Perform intensity projection of the sequence on the skeleton, pixel by pixel."""
import numpy as np
import cv2
from itertools import product
from pyamnesia.utils import timed, save_data
from pyamnesia.signal_processing.trace_processing import normalize_traces, smooth_traces


def get_pixels_coordinates(skeleton_mask):
    """Get the coordinates of the pixels in the skeleton mask.

    Parameters
    ----------
    skeleton_mask : np.array
        Binary image of the skeleton mask.

    Returns
    -------
    pixels_coordinates : np.array
        Coordinates of the components of the entities (pixels). Used to unravel
        the vectors.
    """
    height, width = skeleton_mask.shape

    # Note: we create a 3D array to be consistent with the structure when ``projection_method = branch``
    pixels_coordinates = np.array([np.array([[i, j]]) for (i, j) in product(range(height), range(width)) if skeleton_mask[i, j] == 1], dtype=object)
    return pixels_coordinates


def get_pixels_traces(tif_sequence, pixels_coordinates):
    """Get the traces of the skeleton pixels.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    pixels_coordinates : np.array
        Coordinates of the components of the entities (pixels). Used to unravel
        the vectors.

    Returns
    -------
    pixels_traces : np.array
        Traces of the pixels to analyze.
    """
    pixels_traces = []
    for pixel_coordinates in pixels_coordinates:
        # Note: ``pixel_coordinates`` only has one element, but we adopt the following syntax
        # to be consistent with the structure when ``projection_method = branch``
        for i, j in pixel_coordinates:
            pixels_traces.append(tif_sequence[:, i, j])

    pixels_traces = np.array(pixels_traces)
    return pixels_traces


def get_pixel_sequence(tif_sequence, pixels_traces, pixels_coordinates, dilation_radius=0):
    """Get the sequence projected on the skeleton.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    pixels_traces : np.array
        Traces of the entities pixels to analyze.
    pixels_coordinates : np.array
        Coordinates of the components of the entities (pixels). Used to unravel
        the vectors.
    dilation_radius : int, optional
        The radius of the skeleton. If set to ``0``, the skeleton remains
        one-pixel wide. Otherwise it gets dilated.

    Returns
    -------
    pixel_sequence_dilated : np.array
        Tif sequence projected on the skeleton pixels.
    """
    pixel_sequence = np.zeros_like(tif_sequence)
    for pixel_id, pixel_coordinates in enumerate(pixels_coordinates):
        # Note: ``pixel_coordinates`` only has one element, but we adopt the following syntax
        # to be consistent with the structure when ``projection_method = branch``
        for i, j in pixel_coordinates:
            pixel_sequence[:, i, j] = pixels_traces[pixel_id]

    # Dilate the sequence for visualization
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2*dilation_radius+1, 2*dilation_radius+1))
    pixel_sequence_dilated = cv2.dilate(pixel_sequence, kernel)

    return pixel_sequence_dilated


@timed
def pixel_projection(tif_sequence, projected_mask, config_pixel_projection, config_misc):
    """Perform intensity projection of the sequence on the skeleton, pixel by pixel.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    projected_mask : np.array
        Binary image of the skeleton mask.
    config_pixel_projection : dict
        Config dictionary for `branch_projection`.
        Contains **element_normalization_method**, that can be one of ``'null'``,
        ``'mean-substraction'`` and ``'z-score'``,
        **element_smoothing_window_len**, that is relative to the trace
        smoothing window parameter.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    pixels_coordinates : np.array
        Coordinates of the pixels of the skeleton. Used to unravel the vectors.
    pixels_traces : np.array
        Traces of the skeleton pixels to analyze.
    """
    normalization_method = config_pixel_projection['element_normalization_method']
    smoothing_window_len = config_pixel_projection['element_smoothing_window_len']
    dilation_radius = config_pixel_projection['branch_radius']
    results_path = config_misc['results_path']
    save_projected_sequence = config_misc['save_projected_sequence']

    # Get skeleton pixel coordinates
    pixels_coordinates = get_pixels_coordinates(projected_mask)

    # Get skeleton pixel traces
    pixels_traces = get_pixels_traces(tif_sequence, pixels_coordinates)

    # Save projected sequence
    if save_projected_sequence:
        pixel_sequence = get_pixel_sequence(tif_sequence, pixels_traces,
                                            pixels_coordinates, dilation_radius)
        print("[INFO] Saving skeleton sequence (pixel)...")
        save_data(pixel_sequence, 'tif', results_path, "sequence_projection", "projected_sequence.tif")

    # Smooth traces
    pixels_traces = smooth_traces(pixels_traces, window_len=smoothing_window_len)

    # Normalize traces
    pixels_traces = normalize_traces(pixels_traces, normalization_method=normalization_method)

    return pixels_coordinates, pixels_traces
