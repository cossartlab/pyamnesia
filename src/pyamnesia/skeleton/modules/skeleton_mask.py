"""Perfom image skeletonization on the sequence."""
import os
import numpy as np
import cv2
from scipy.ndimage import gaussian_filter
from skimage.morphology import skeletonize, remove_small_objects
from skimage.measure import label
from skimage.color import label2rgb
from pyamnesia.utils import timed, save_data


def summarize_sequence(tif_sequence, config_summarize, config_misc):
    """Extract a summary image from the movie.

    Three methods are implemented:
        - ``'mean'``: get the mean image along the time axis.
        - ``'median'``: get the median image along the time axis.
        - ``'standard deviation'``: get the standard deviation image along the
        time axis.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    config_summarize : dict
        Config dictionary for `summarize`.
        Contains **summary_method**, that can be one of ``'mean'``,
        ``'median'`` and ``'standard_deviation'``, **std_grouping_ratio**, that
        is the number of sequence frames to group before computing the sequence
        standard deviation in order to remove noise from the summary image.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    summary_image : np.array
        Image that summarizes the ``.tif`` sequence, following the
        **summarize_sequence** method.

    Raises
    ------
    NotImplementedError
        When a method name isn't related to any implemented method.
    """
    summary_method = config_summarize['summary_method']
    std_grouping_ratio = config_summarize['std_grouping_ratio']
    results_path = config_misc['results_path']

    if summary_method == 'mean':
        summary_image = np.mean(tif_sequence, axis=0)
    elif summary_method == 'median':
        summary_image = np.median(tif_sequence, axis=0)
    elif summary_method == 'standard_deviation':
        # Group the frames and average them to remove the noise
        tif_sequence_avg = np.mean(tif_sequence.reshape(-1, std_grouping_ratio, tif_sequence.shape[1], tif_sequence.shape[2]), axis=1)
        summary_image = np.std(tif_sequence_avg, axis=0)
    else:
        raise NotImplementedError(f"The summary method {summary_method} hasn't been implemented yet.")

    # Convert dtype to ``uint8``
    # Necessary to use OpenCV image processing methods
    summary_image = ((summary_image / summary_image.max()) * 255).astype(np.uint8)

    # Save the summary image in ``.png`` format
    save_data(summary_image, 'image', results_path, "skeleton_mask", "0_summary_image.png")

    return summary_image


def skeletonize_image(summary_image, config_skeletonize, config_misc):
    """Perform morphological operations to get the skeleton of the summary image.

    Are succesively performed:
        - histogram equalization
        - gaussian smoothing
        - adaptive thresholding
        - holes removing
        - skeletonization (Lee)
        - parasitic skeletons removal

    Parameters
    ----------
    summary_image : np.array
        Image that summarizes the ``.tif`` sequence, following the
        **summarize_sequence** method.
    config_skeletonize : dict
        Config dictionary for `summarize`.
        Contains **projection_substructure**, that can be either ``'skeleton'``
        or ``'active'``, **sigma_smoothing**, that is the standard deviation in
        the gaussian smoothing, **adaptive_threshold_block_size**, that is the
        block size in ``OpenCV``'s ``adaptiveThreshold`` function,
        **holes_max_size**, that is the maximum size of holes in the thresholded
        image to be filled, and **parasitic_skeletons_size**, that is the size
        under which isolated skeletons are considered as noise.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    skeleton_mask : np.array
        Binary image of the skeleton mask.

    Raises
    ------
    NotImplementedError
        When a method name isn't related to any implemented method.
    """
    projection_substructure = config_skeletonize['projection_substructure']
    sigma_smoothing = config_skeletonize['sigma_smoothing']
    adaptive_threshold_block_size = config_skeletonize['adaptive_threshold_block_size']
    holes_max_size = config_skeletonize['holes_max_size']
    parasitic_skeletons_size = config_skeletonize['parasitic_skeletons_size']
    results_path = config_misc['results_path']

    # Histogram equalization
    im_eq = cv2.equalizeHist(summary_image)

    # Gaussian smoothing
    im_smooth = gaussian_filter(im_eq, sigma=sigma_smoothing)

    # Gaussian adaptive thresholding
    im_thresh = cv2.adaptiveThreshold(im_smooth, 1, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,
                                      adaptive_threshold_block_size, 0)

    # Holes filling
    im_thresh_clean = (1 - remove_small_objects((1 - im_thresh).astype(bool), holes_max_size, connectivity=1)).astype(
        np.uint8)

    # Skeletonization (Lee)
    im_skel = skeletonize(im_thresh_clean, method='lee')

    # Parasitic skeletons removal
    im_skel_clean = remove_small_objects(im_skel.astype(bool), parasitic_skeletons_size, connectivity=2)

    # Skeleton labelling for visualization
    im_label = label(im_skel_clean)
    im_label_overlay = label2rgb(im_label, image=im_eq, alpha=0.6, image_alpha=1, bg_label=0)

    # Save intermediate images to understand the parameters influence
    print("[INFO] Saving intermediate skeletonization images...")
    save_data(summary_image, 'image', results_path, "sequence_projection", "summary_image.png")
    save_data(im_eq, 'image', results_path, "skeleton_mask", "1_histogram_equalization.png")
    save_data(im_smooth, 'image', results_path, "skeleton_mask", "2_smoothing.png")
    save_data(im_thresh * 255, 'image', results_path, "skeleton_mask", "3_thresholding.png")
    save_data(im_thresh_clean * 255, 'image', results_path, "skeleton_mask", "4_holes_removing.png")
    save_data(im_skel * 255, 'image', results_path, "skeleton_mask", "5_skeletonization.png")
    save_data(im_skel_clean * 255, 'image', results_path, "skeleton_mask", "6_skeleton_cleaning.png")
    save_data(im_label_overlay * 255, 'image', results_path, "skeleton_mask", "7_skeleton_label.png")

    if projection_substructure == 'active':
        # Return the binary image as the mask
        save_data(im_thresh_clean, 'image', results_path, "sequence_projection", "projected_mask.png")
        return im_thresh_clean
    elif projection_substructure == 'skeleton':
        # Return the skeleton as the mask
        save_data(im_skel_clean.astype(np.uint8), 'image', results_path, "sequence_projection", "projected_mask.png")
        return im_skel_clean.astype(np.uint8)
    else:
        raise NotImplementedError(f"The projection sub-structure {projection_substructure} has not been implemented")


@timed
def get_skeleton_mask(tif_sequence, config_skeleton_mask, config_misc):
    """Perfom image skeletonization on the sequence.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    config_skeleton_mask :
        Config dictionary for `skeleton_mask`.
        Contains the parameters relative to **summary image** generation and **image skeletonization**.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    summary_image : np.array
        Image that summarizes the ``.tif`` sequence, following the
        **summarize_sequence** method.
    skeleton_mask : np.array
        Binary image of the skeleton mask.

    """
    # Get the summary image
    summary_image = summarize_sequence(tif_sequence,
                                       config_skeleton_mask['summarize'],
                                       config_misc)
    # Get the mask
    skeleton_mask = skeletonize_image(summary_image,
                                      config_skeleton_mask['skeletonize'],
                                      config_misc)

    return summary_image, skeleton_mask
