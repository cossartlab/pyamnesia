"""Perform branch validation : a method to get the branches that uniformly
illuminate during transients."""
import numpy as np
from scipy.stats import skew
from pyamnesia.signal_processing.transient_analysis import get_transients_frames
from scipy.ndimage import gaussian_filter
from pyamnesia.utils import timed, save_data
import matplotlib.pyplot as plt


def get_branch_bounds(tif_sequence, branch_coordinates, pixels_around=5):
    """Summary

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    branch_coordinates : np.array
        Coordinates of the components of one branch. Used to unravel the
        vectors.
    pixels_around : int, optional
        Number of pixels to display around the analyzed branch, for
        visualization.

    Returns
    -------
    branch_bounds : tuple
        Coordinates of the bounding box around the branch.
    """
    len_frame_x = tif_sequence[0].shape[1]
    len_frame_y = tif_sequence[0].shape[0]

    # Determine the coordinates of the square surrounding the branch
    min_x = min(branch_coordinates, key=lambda t: t[1])[1]
    min_y = min(branch_coordinates, key=lambda t: t[0])[0]
    max_x = max(branch_coordinates, key=lambda t: t[1])[1]
    max_y = max(branch_coordinates, key=lambda t: t[0])[0]

    # Add pixels around the bounding box
    min_x = max(0, min_x - pixels_around)
    min_y = max(0, min_y - pixels_around)
    max_x = min(len_frame_x - 1, max_x + pixels_around)
    max_y = min(len_frame_y - 1, max_y + pixels_around)

    branch_bounds = (min_x, min_y, max_x, max_y)
    return branch_bounds


def get_branch_mask(branch_bounds, branch_coordinates):
    """Get the binary mask of the branch.

    Parameters
    ----------
    branch_bounds : tuple
        Coordinates of the bounding box around the branch.
    branch_coordinates : np.array
        Coordinates of the components of one branch. Used to unravel the
        vectors.

    Returns
    -------
    branch_mask : np.array
        The binary image of the branch.
    """
    min_x, min_y, max_x, max_y = branch_bounds
    branch_shape = (max_y - min_y + 1, max_x - min_x + 1)

    branch_mask = np.zeros(branch_shape)
    for i, j in branch_coordinates:
        branch_mask[i - min_y][j - min_x] = 1

    return branch_mask


def get_transients_profiles(tif_sequence, branch_bounds, branch_trace, peak_properties=None, transient_neighborhood=2):
    """Get the transient profiles of the branch.

    A transient profile is defined as the cropped frame of the branch during a
    transient, with weighted intensity averaging around the transient peak
    frame.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    branch_bounds : tuple
        Coordinates of the bounding box around the branch.
    branch_trace : np.array
        Trace of the branch to analyze.
    peak_properties : dict, optional
        Dictionary to parametrize the traces peaks detection.
        Contains **height**, **threshold**, **distance**, **prominence**,
        **width**. For more details, please refer to the
        ``scipy.signal.find_peaks`` function.
    transient_neighborhood : int, optional
        Number of frames between transients onsets and peaks.
        It is set by default to 2, but one could implement a method to detect
        the real transients' onsets and offsets frames.

    Returns
    -------
    transients_profiles : list
        List of a branch transients profiles.
    transients_intensities : np.array
        Array of transients intensities. We take the positive intensities,
        `i.e.` we add the absolute value of the trace minimum to make it positive.
    transients_frames : list
        List of transients frame numbers.

    References
    ----------
    - ``scipy.signal.find_peaks``
    """
    min_x, min_y, max_x, max_y = branch_bounds

    # Get the frame indices of the the branch transients
    transients_frames = get_transients_frames(branch_trace, peak_properties=peak_properties)

    # Add a constant to make the trace positive
    branch_trace_pos = branch_trace + abs(np.min(branch_trace))
    transients_intensities = branch_trace_pos[transients_frames]

    transients_profiles = []
    for transient_frame in transients_frames:
        # Note: we assume that the number of frames between the onsets and peaks and peaks and offsets is constant,
        # which is false, but it doesn't skew the results.
        onset = max(0, transient_frame - transient_neighborhood)
        offset = min(len(tif_sequence) - 1, transient_frame + transient_neighborhood)
        onset_to_offset_trace = branch_trace_pos[onset: offset + 1]
        transient_profile = np.average(tif_sequence[onset:offset + 1, min_y:max_y + 1, min_x:max_x + 1],
                                       weights=onset_to_offset_trace, axis=0)
        transient_profile = gaussian_filter(transient_profile, sigma=1)
        transients_profiles.append(transient_profile)

    return transients_profiles, transients_intensities, transients_frames


def flatten_profile(transient_profile, branch_mask):
    """Convert a transient profile to a flat array containing only the pixels
    of the branch mask.

    Parameters
    ----------
    transient_profile : np.array
        A branch transient profile.
    branch_mask : np.array
        The binary image of the branch.

    Returns
    -------
    flat_profile : np.array
        The transient profile values flattened along the branch_mask.
    """
    flat_profile = []
    for i in range(branch_mask.shape[0]):
        for j in range(branch_mask.shape[1]):
            if branch_mask[i, j] == 1:
                flat_profile.append(transient_profile[i, j])

    flat_profile = np.array(flat_profile)
    return flat_profile


def get_transient_profile_skewness(branch_mask, transient_profile):
    """Get the skewness of a transient profile.

    Parameters
    ----------
    branch_mask : np.array
        The binary image of the branch.
    transient_profile : np.array
        A branch transient profile.

    Returns
    -------
    transient_skewness : int
        The branch transient skewness.
    """
    transient_profile_1d = flatten_profile(transient_profile, branch_mask)
    transient_skewness = skew(transient_profile_1d)
    return transient_skewness


def get_branch_skewness(branch_mask, transients_profiles, transients_intensities):
    """Get the list of skewnesses of a branch during its transients.

    Parameters
    ----------
    branch_mask : np.array
        The binary image of the branch.
    transients_profiles : list
        List of a branch transients profiles.
    transients_intensities : np.array
        Array of transients intensities.

    Returns
    -------
    branch_skewness : int
        The branch average skewness over the transients, weighted by the
        transients intensities.
    """
    stats_skewness = []
    for transient_profile in transients_profiles:
        transient_skewness = get_transient_profile_skewness(branch_mask, transient_profile)
        stats_skewness.append(transient_skewness)
    branch_skewness = np.average(np.array(stats_skewness), weights=transients_intensities)
    return branch_skewness


def plot_branch_stats(branch_id, branch_bounds, branch_mask, transients_profiles, transients_frames, branch_skewness, config_misc):
    """Plot stats about the branch.

    Plots :
        - transients frames
        - transients intensities
        - transients skewnesses
        - transients profiles

    Parameters
    ----------
    branch_id : int
        The branch id in the list of branches.
    branch_bounds : tuple
        Coordinates of the bounding box around the branch.
    branch_mask : np.array
        The binary image of the branch.
    transients_profiles : list
        List of a branch transients profiles.
    transients_frames : list
        List of transients frame numbers.
    branch_skewness : int
        The branch average skewness over the transients, weighted by the
        transients intensities.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.
    """
    results_path = config_misc['results_path']

    min_x, min_y, max_x, max_y = branch_bounds
    nb_transients = len(transients_profiles)

    # Get mask overlay
    branch_mask_overlay = np.ma.masked_where(branch_mask == 0, branch_mask)

    plt.ioff()
    fig, (axes_profiles, axes_hist) = plt.subplots(2, 5, figsize=(10, 8))
    fig.suptitle(f"C(x{(min_x + max_x) // 2},y{(min_y + max_y) // 2}) ; "
                 f"{nb_transients} transients ; "
                 f"Global Skewness = {branch_skewness:.1f}")

    # Remove empty axes
    for ax_profile, ax_hist in zip(axes_profiles.flatten(), axes_hist.flatten()):
        ax_profile.axis('off')
        ax_hist.axis('off')

    for index, (transient_profile, transient_frame) in enumerate(zip(transients_profiles[:5], transients_frames[:5])):

        ax_profile = axes_profiles[index]
        ax_hist = axes_hist[index]
        ax_profile.axis('on')
        ax_hist.axis('on')

        transient_profile_skewness = get_transient_profile_skewness(branch_mask, transient_profile)
        ax_profile.set_title(f"T={transient_frame}")
        ax_profile.imshow(transient_profile, cmap='gray')
        ax_profile.imshow(branch_mask_overlay, cmap='rainbow', alpha=.2)

        ax_hist.set_title(f"Sk={transient_profile_skewness:.1f}")
        transient_profile_1d = flatten_profile(transient_profile, branch_mask)
        transient_profile_1d = ((transient_profile_1d / transient_profile_1d.max()) * 255).astype(np.uint8) #for visualization
        ax_hist.hist(transient_profile_1d, bins=int(256 / 5), range=(0, 255))
        ax_hist.get_xaxis().set_visible(False)

    save_data(fig, 'figure', results_path, "branch_validation", f"branch_stats_{branch_id}.png")
    plt.close(fig)


def validate_branch(tif_sequence, branch_coordinates, branch_trace, branch_id, n_branches, config_misc, pixels_around=5, peak_properties={}, transient_neighborhood=2):
    """Perform the validation of one branch.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    branch_coordinates : np.array
        Coordinates of the components of one branch. Used to unravel the
        vectors.
    branch_trace : np.array
        Trace of the branch to analyze.
    branch_id : int
        The branch id in the list of branches.
    n_branches : int
        The total number of branches in the skeleton.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.
    pixels_around : int, optional
        Number of pixels to display around the analyzed branch, for visualization.
    peak_properties : dict, optional
        Dictionary to parametrize the traces peaks detection.
        Contains **height**, **threshold**, **distance**, **prominence**,
        **width**. For more details, please refer to the
        ``scipy.signal.find_peaks`` function.
    transient_neighborhood : int, optional
        Number of frames between transients onsets and peaks.
        It is set by default to 2, but one could implement a method to detect
        the real transients' onsets and offsets frames.

    Returns
    -------
    branch_skewness : int
        The branch average skewness over the transients.

    References
    ----------
    - ``scipy.signal.find_peaks``
    """

    # Get branch bounds
    branch_bounds = get_branch_bounds(tif_sequence, branch_coordinates,
                                      pixels_around=pixels_around)
    # Get branch mask
    branch_mask = get_branch_mask(branch_bounds, branch_coordinates)

    # Get transients profiles
    transients_profiles, transients_intensities, transients_frames = get_transients_profiles(tif_sequence,
                                                                                             branch_bounds,
                                                                                             branch_trace,
                                                                                             peak_properties=peak_properties,
                                                                                             transient_neighborhood=transient_neighborhood)
    # In case there isn't any transient
    if len(transients_profiles) == 0:
        return -1

    # Get branch weighted skewness
    branch_skewness = get_branch_skewness(branch_mask, transients_profiles, transients_intensities)

    # Plot only 50 equally spaced branches
    if branch_id in np.rint(np.linspace(0, n_branches, 50)).astype(int):
        plot_branch_stats(branch_id, branch_bounds, branch_mask, transients_profiles, transients_frames,
                          branch_skewness, config_misc)

    return branch_skewness


def plot_skewness_hist(stats_skewnesses, skewness_threshold, nb_valid_branches, nb_morphological_branches, results_path):
    """Plot the histogram of skewnesses distribution for all the branches.

    Parameters
    ----------
    stats_skewnesses : list
        The list of all branches skewnesses. Used to get the skewness
        distribution histogram.
    skewness_threshold : int
        The skewness value, above which a branch isn't considered as valid.
    nb_valid_branches : int
        The total number of valid branches.
    nb_morphological_branches : int
        The total number of branches.
    results_path : string
        Path to where the results are stored.
    """
    plt.ioff()
    fig, ax = plt.subplots(1, 1)
    ax.hist(stats_skewnesses, bins=50, color='blue', edgecolor='black')
    ax.axvline(x=skewness_threshold, color='red', ls='--')
    ax.set_title('skewness distribution\n'
                 f'valid branches : {nb_valid_branches}/{nb_morphological_branches}')
    print()
    print("[INFO] Saving branch skewness histogram...")
    save_data(fig, 'figure', results_path, "branch_validation", "distribution_skewness")
    plt.close(fig)


@timed
def validate_branches(tif_sequence, branches_coordinates, branches_traces, config_validate_branches, config_misc):
    """Perform branch validation.

    A method to get the branches that uniformly illuminate during transients.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    branches_coordinates : np.array
        Coordinates of the components of the branches. Used to unravel the
        vectors.
    branches_traces : np.array
        Traces of the branches to analyze.
    config_validate_branches : dict
        Config dictionary for `validate_branches`.
        Contains **pixels_around**, **peak_properties**, **skewness_threshold**
        and **transient_neighborhood**.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    valid_branches_coordinates : np.array
        Coordinates of the components of the valid branches. Used to unravel
        the vectors.
    valid_branches_traces : np.array
        Traces of the valid branches to analyze.
    """
    pixels_around = config_validate_branches['pixels_around']
    peak_properties = config_validate_branches['peak_properties']
    transient_neighborhood = config_validate_branches['transient_neighborhood']
    skewness_threshold = config_validate_branches['skewness_threshold']
    results_path = config_misc['results_path']

    n_branches = len(branches_coordinates)
    valid_branches_coordinates = []
    valid_branches_traces = []
    stats_skewnesses = []
    for branch_id, (branch_coordinates, branch_trace) in enumerate(zip(branches_coordinates, branches_traces)):
        if branch_id % (n_branches // 10) == 0:
            print(".", end='', flush=True)

        branch_skewness = validate_branch(tif_sequence, branch_coordinates, branch_trace, branch_id, n_branches,
                                          config_misc,
                                          pixels_around=pixels_around,
                                          peak_properties=peak_properties,
                                          transient_neighborhood=transient_neighborhood)

        stats_skewnesses.append(branch_skewness)
        if branch_skewness <= skewness_threshold:
            valid_branches_coordinates.append(branch_coordinates)
            valid_branches_traces.append(branch_trace)

    valid_branches_coordinates = np.array(valid_branches_coordinates, dtype=object)
    valid_branches_traces = np.array(valid_branches_traces)

    plot_skewness_hist(stats_skewnesses, skewness_threshold, len(valid_branches_coordinates), len(branches_coordinates),
                       results_path)
    return valid_branches_coordinates, valid_branches_traces
