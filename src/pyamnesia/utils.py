"""Some useful functions used in several scripts.

Includes data manipulation, data loading and data saving functions, exceptions
and other tools.
"""
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import cv2
from time import time  # for the `timed` decorator
from tifffile import imread, imsave  # to read tif files


def read_tif(path_to_image, config_read_tif=None):
    """Read a .tif sequence.

    Parameters
    ----------
    path_to_image :
        Path to the .tif sequence.
    config_read_tif : dict, optional
        Config dictionary for `read_tif`. Contains **grouping_ratio**, that
        performs a temporal grouping of the sequence by computing the mean of
        the frames over a certain number, and **crop**, that reduces the
        spatial dimensions of the sequence.

    Returns
    -------
    tif_sequence : np.array
        Array of the .tif sequence.
    """
    if config_read_tif is None:
        grouping_ratio = 1
        crop = 0
    else:
        grouping_ratio = config_read_tif['grouping_ratio']
        crop = config_read_tif['crop']
    tif_sequence = imread(path_to_image)
    if tif_sequence.ndim == 2:
        tif_sequence = tif_sequence[np.newaxis, :, :]
    if grouping_ratio > 1:
        # test if grouping_ratio is a divider of number of frames
        if tif_sequence.shape[0] % grouping_ratio != 0:
            print(f"Warning! Number of frames ({tif_sequence.shape[0]}) is not a multiple of grouping_ratio ({grouping_ratio})")
        tif_sequence = np.mean(tif_sequence.reshape(-1, grouping_ratio, tif_sequence.shape[1], tif_sequence.shape[2]),
                               axis=1)
    if crop > 0:
        # test if crop is too high or not
        if 2 * crop > min(tif_sequence.shape[1:]):
            print(f"Warning! Crop ({crop}) is too high compared to the image dimensions ({tif_sequence.shape[1:]})")
            crop = min(tif_sequence.shape[1:])
        tif_sequence = tif_sequence[:, crop:-crop, crop:-crop]

    return tif_sequence.astype(np.float32)


def save_data(data, data_type, path_to_save, folder_name, file_name):
    """Save an object depending on its type.

    Parameters
    ----------
    data :
        Object to save.
    data_type :
        Type of object.
    path_to_save, folder_name, file_name :
        Object will be saved at
        os.path.join(path_to_save, folder_name, file_name)
    """
    data_path = os.path.join(path_to_save, folder_name, file_name)
    if not os.path.exists(os.path.dirname(data_path)):
        os.makedirs(os.path.dirname(data_path))

    data_path = os.path.join(path_to_save, folder_name, file_name)
    if data_type == 'tif':
        imsave(data_path, data)
    elif data_type == 'image':
        cv2.imwrite(data_path, data)
    elif data_type == 'image_plt':
        plt.imsave(data_path, data, cmap='gray')
    elif data_type == 'figure':
        plt.savefig(data_path)
    elif data_type == 'npy':
        np.save(data_path, data)
    elif data_type == 'csv':
        data.to_csv(data_path)
    elif data_type == 'xlsx':
        data.to_excel(data_path)
    else:
        print("Couldn't save the data : unknown type")


def matrices_to_vectors(matrices):
    """Transform a vector of matrices into their flattened version.

    Useful for considering an image as a statistical entity and for
    performing matrix factorization techniques.

    Parameters
    ----------
    matrices : np.array
        Vector of matrices to flatten.

    Returns
    -------
    vectors np.array
        Array of flattened matrices.
    """
    nb_frames, height, width = matrices.shape
    vectors = matrices.reshape((nb_frames, height * width))
    return vectors


def vectors_to_matrices(vectors, height, width, unravel_list=None):
    """Transform a vector of vectors into their unraveled version.

    Useful for getting back to the original space after using the
    ``matrices_to_vectors`` function.
    If the **unravel_list** argument is specified, it is used to affect each
    pixel to its original 2D coordinates. If not, a simple reshape is performed.

    Parameters
    ----------
    vectors : np.array
        Vector of vectors to unravel.
    height : int
        Height of the output images.
    width : int
        Width of the output images.
    unravel_list : list, optional
        List of lists of coordinates. For each skeleton element (pixel, branch),
        gives the coordinates of the pixels it is composed of.

    Returns
    -------
    matrices : np.array
        Array of unraveled vectors.
    """
    nb_frames, nb_pixels = vectors.shape
    if unravel_list is None:  # simple reshape
        assert nb_pixels == height * width, f"Dimensions don't match: {height}x{width}!={nb_pixels}"
        matrices = vectors.reshape((nb_frames, height, width))
    else:  # use pixels coordinates
        matrices = np.zeros((nb_frames, height, width))
        for skeleton_element_id, skeleton_element_coords in enumerate(unravel_list):
            for i, j in skeleton_element_coords:
                matrices[:, i, j] = vectors[:, skeleton_element_id]  # trace of pixel (i, j)
    return matrices


def timed(fun):
    """Decorator for timing important functions."""
    def timed_fun(*args, **kwargs):
        print(f"=== {fun.__name__.upper()} ===")
        ts = time()
        result = fun(*args, **kwargs)
        te = time()
        print(f"[done in {te - ts:0.2f}s]\n")
        return result

    return timed_fun


class Logger(object):

    """Logger for printing both in terminal and in log.txt file.

    Attributes
    ----------
    file :
        Opened output file.
    stdout :
        System standard output.
    """

    def __init__(self, name, mode):
        self.file = open(name, mode)
        self.stdout = sys.stdout
        sys.stdout = self

    def __del__(self):
        sys.stdout = self.stdout
        self.file.close()

    def write(self, data):
        self.file.write(data)
        self.stdout.write(data)

    def flush(self):
        self.file.flush()


class NoInputError(Exception):

    """When no input is selected in a dialog box."""

    def __init__(self):
        print("No input selected\nLeaving tool...")
        sys.exit(0)


class NoDirectoryError(Exception):

    """When no directory is selected in a dialog box."""

    def __init__(self):
        print("No directory selected\nLeaving tool...")
        sys.exit(0)


class NoClusterError(Exception):

    """When no cluster can be found."""

    def __init__(self):
        print("No cluster found: please update t-SNE and HDBSCAN parameters\nLeaving tool...")
        sys.exit(0)


class NotNonNegativeError(Exception):

    """When the NMF input is not nonnegative."""

    def __init__(self):
        print("Frames are not nonnegative. Please change the `element_normalization_method` to null.\nLeaving tool...")
        sys.exit(0)
