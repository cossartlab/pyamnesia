"""Clustering analysis for CICADA."""
import numpy as np
import copy
import cv2
from time import time
from cicada.analysis.cicada_analysis import CicadaAnalysis
# enabling pyAMNESIA imports from CICADA
import os
analyses_dir = os.path.dirname(__file__)
pyamnesia_dir = os.path.abspath(os.path.join(analyses_dir, '../../..'))  # src
import sys
sys.path.append(pyamnesia_dir)
# pyAMNESIA imports
from pyamnesia.utils import save_data
from pyamnesia.gui.format_config import format_clustering_config
from pyamnesia.correlation.clustering.clustering import clustering


class CicadaPyamnesiaClusteringAnalysis(CicadaAnalysis):

    """Clustering analysis for CICADA.

    See also
    --------
    cicada.analysis.ci_analyses
    """

    def __init__(self, config_handler=None):
        """Class initialization."""
        CicadaAnalysis.__init__(self, name="Clustering", family_id="pyAMNESIA",
                                short_description="Clusterize pixels or branches",
                                long_description="The ``clustering`` module provides a clustering pipeline to group coactive elements in a ``.tif`` sequence.\nFor more information, please visit pyAMNESIA documentation (https://pyamnesia.readthedocs.io/).",
                                config_handler=config_handler,
                                accepted_data_formats=["CI_TIF_DATA"])

    def copy(self):
        """Make a copy of the analysis.

        Returns
        -------
        analysis_copy
            Copy of the analysis.
        """
        analysis_copy = CicadaPyamnesiaClusteringAnalysis(config_handler=self.config_handler)
        self.transfer_attributes_to_tabula_rasa_copy(analysis_copy=analysis_copy)
        return analysis_copy

    def check_data(self):
        """Check the data given and return ``True`` if the data given allows
        the analysis implemented, ``False`` otherwise.

        Returns
        -------
        bool
            If the data given allows the analysis implemented.
        """
        super().check_data()

        for session_index, session_data in enumerate(self._data_to_analyse):
            if session_data.DATA_FORMAT != "CI_TIF_DATA":
                self.invalid_data_help = f"Non CI_TIF_DATA format compatibility not yet implemented: " \
                                         f"{session_data.DATA_FORMAT}"
                return False

        return True

    def set_arguments_for_gui(self):
        """Add the CICADA arguments for the clustering analysis."""
        CicadaAnalysis.set_arguments_for_gui(self)

        self.add_open_dir_dialog_arg_for_gui(
            arg_name="path_to_skeleton_data",
            mandatory=True,
            short_description="Skeleton session folder",
            long_description="Folder containing the skeleton session results on which we want to perform trace clustering.",
            family_widget="skeleton"
        )
        # ------ dimensionality reduction ----- #
        self.add_choices_arg_for_gui(
            arg_name="normalization_method",
            choices=[None, "mean-substraction", "z-score"],
            short_description="Method used to normalize the traces",
            long_description="The ``normalization_method`` parameter selects the method for normalizing the traces. The choices are:\n- ``'null'``: no normalization;\n- ``'mean-substraction'``;\n- ``'z-score'``.\nThe normalization is done again to make sure the dimensionality reduction is under the right format. If the traces were already :ref:`normalized during skeletonization<element-normalization-method>`, there is no need to do it again.",
            default_value="z-score",
            multiple_choices=False,
            family_widget="dimensionality_reduction"
            )
        self.add_field_float_option_for_gui(
            arg_name="pca_variance_goal",
            short_description="PCA variance goal",
            long_description="The ``pca_variance_goal`` parameter selects the **percentage** of variance to keep after PCA. It must be a ``float`` inferior to ``1``.\nThe higher it is, the more principal components are kept.\nThe advantage of setting a percentage goal, is that you keep control on the level of information you want to keep on the traces. You can also input an ``integer``, if you know the exact number of components you want to give to the following tsne algorithm. But be wary, you might loose a lot of information if you se it too low.",
            mandatory=True,
            default_value=0.99,
            family_widget="dimensionality_reduction"
            )
        self.add_field_text_option_for_gui(
            arg_name="tsne_distance_metric",
            short_description="TSNE distance metric",
            long_description="The ``tsne_distance_metric`` parameter selects the distance metric for t-SNE distance matrix computation.\nIt can be one of ``pearson``, ``spearman`` and every metric in``sklearn.metrics.pairwise.distance_metrics``.\nValues:\n - pearson\n - spearman\n - l2\n - etc... (sklearn.metrics.pairwise.distance_metrics)",
            default_value="spearman",
            family_widget="dimensionality_reduction"
            )
        self.add_int_values_arg_for_gui(
            arg_name="tsne_perplexity",
            min_value=1,
            max_value=60,
            short_description="TSNE perplexity",
            long_description="The ``tsne_perplexity`` parameter selects the 'number of nearest neighbors that is used in other manifold learning algorithms' (source : TSNE's official documentation). The value needs to take into account the typical size of clusters we want. For instance, if we want to cluster skeleton pixel components, the perplexity needs to be much higher than if we want to cluster branches.",
            default_value=30,
            family_widget="dimensionality_reduction"
            )
        self.add_int_values_arg_for_gui(
            arg_name="tsne_random_state",
            min_value=0,
            max_value=100,
            short_description="TSNE random state",
            long_description="The ``tsne_random_state`` parameter selects the random number generator for TSNE algorithm.\nFor reproducible results, pass an int.",
            default_value=42,
            family_widget="dimensionality_reduction"
            )
        # ------ HDBSCAN clustering ----- #
        self.add_field_text_option_for_gui(
            arg_name="hdbscan_metric",
            short_description="HDBSCAN metric",
            long_description="values:\n - l2\n - etc. (hdbscan.dist_metrics.METRIC_MAPPING).",
            default_value="l2",
            family_widget="hdbscan_clustering"
            )
        self.add_int_values_arg_for_gui(
            arg_name="min_cluster_size",
            min_value=1,
            max_value=100,
            short_description="Minimum cluster size",
            long_description="The ``min_cluster_size`` parameter selects the minimum size of clusters.",
            default_value=5,
            family_widget="hdbscan_clustering"
            )
        self.add_int_values_arg_for_gui(
            arg_name="min_samples",
            min_value=1,
            max_value=100,
            short_description="Minimum samples",
            long_description="The ``min_samples`` parameter selects 'the number of samples in a neighbourhood for a point to be considered a core point' (source : HDBSCAN API).",
            default_value=5,
            family_widget="hdbscan_clustering"
            )

        self.add_image_format_package_for_gui()

        self.add_with_timestamp_in_filename_arg_for_gui()


    def update_original_data(self):
        """To be called if the data to analyse should be updated after the
        analysis has been run.

        Returns
        -------
        bool
            ``True`` if the data has been modified
        """
        pass

    def run_analysis(self, **kwargs):
        """Run the clustering analysis.

        Parameters
        ----------
        **kwargs : dict
            Config dictionary for clustering.
        """
        CicadaAnalysis.run_analysis(self, **kwargs)

        # format kwargs
        verbose = True
        config_misc, config_clustering = format_clustering_config(kwargs)
        config_misc['results_path'] = self.get_results_path()  # security

        # start runs
        n_sessions = len(self._data_to_analyse)
        plural = "s" if n_sessions >= 2 else ""
        print(f"{n_sessions} session{plural}")
        path_to_skeleton_data = kwargs['path_to_skeleton_data']

        for session_index, session_data in enumerate(self._data_to_analyse):
            session_identifier = session_data.identifier
            print(f"-------------- {session_identifier} -------------- ")

            # update results path
            config_misc['results_path'] = os.path.join(self.get_results_path(), session_identifier)

            # get skeleton results
            path_to_skeleton_data_session = os.path.join(path_to_skeleton_data, session_identifier)

            # check if files are present
            file_folders = ['sequence_projection', 'sequence_projection', 'sequence_projection', 'sequence_projection']
            file_names = ['projected_traces.npy', 'projected_coordinates.npy', 'projected_mask.png', 'summary_image.png']
            files_are_present = True
            for file_folder, file_name in zip(file_folders, file_names):
                if not os.path.isfile(os.path.join(path_to_skeleton_data_session, file_folder, file_name)):
                    print(f"Unable to find a skeleton result ({file_name}).")
                    files_are_present = False
                    break
            if not files_are_present:
                print(f"Skipping {session_identifier}.\n")
                continue

            # load data
            projected_traces = np.load(os.path.join(path_to_skeleton_data_session, 'sequence_projection', 'projected_traces.npy'))
            projected_coordinates = np.load(os.path.join(path_to_skeleton_data_session, 'sequence_projection', 'projected_coordinates.npy'), allow_pickle=True)
            projected_mask = cv2.imread(os.path.join(path_to_skeleton_data_session, 'sequence_projection', 'projected_mask.png'), flags=cv2.IMREAD_GRAYSCALE)
            summary_image = cv2.imread(os.path.join(path_to_skeleton_data_session, 'sequence_projection', 'summary_image.png'), flags=cv2.IMREAD_GRAYSCALE)

            results_path = config_misc['results_path']
            save_data(projected_mask, 'image', results_path, "sequence_projection", "projected_mask.png")
            save_data(summary_image, 'image', results_path, "sequence_projection", "summary_image.png")

            clusters_masks, clusters_coordinates, clusters_mean_traces = clustering(
                projected_traces=projected_traces,
                projected_coordinates=projected_coordinates,
                summary_image=summary_image,
                projected_mask=projected_mask,
                config_clustering=config_clustering,
                config_misc=config_misc,
            )

            if verbose:
                print(" ")
            self.update_progressbar(time_started=self.analysis_start_time, increment_value=100 / (n_sessions+1))

        print(f"Clustering analysis run in {time() - self.analysis_start_time:.2f} sec")

