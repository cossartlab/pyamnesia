"""Factorization analysis for CICADA."""
import numpy as np
from time import time
from cicada.analysis.cicada_analysis import CicadaAnalysis
# enabling pyAMNESIA imports from CICADA
import os
analyses_dir = os.path.dirname(__file__)
pyamnesia_dir = os.path.abspath(os.path.join(analyses_dir, '../../..'))  # src
import sys
sys.path.append(pyamnesia_dir)
# pyAMNESIA imports
from pyamnesia.gui.format_config import format_factorization_config
from pyamnesia.correlation.factorization.factorization import factorization


class CicadaPyamnesiaFactorizationAnalysis(CicadaAnalysis):

    """Factorization analysis for CICADA.

    See also
    --------
    cicada.analysis.ci_analyses
    """

    def __init__(self, config_handler=None):
        """Class initialization."""
        CicadaAnalysis.__init__(self, name="Factorization", family_id="pyAMNESIA",
                                short_description="Apply matrix factorization techniques",
                                long_description="The analysis can perform several matrix factorization techniques such as PCA (Principal Components Analysis), ICA (Independent Components Analysis) and NMF (Nonnegative Matrix Factorization).\nIf you choose to perform an ICA, a dimensionality reduction is needed. For this, you can either perform a PCA, or use an autoencoder. After either a NMF or a PCA-ICA, components are selected by their skewness.\nYou can get information on each parameter of the analysis by clicking on the '?'.\nFor more information, please visit pyAMNESIA documentation (https://pyamnesia.readthedocs.io/).",
                                config_handler=config_handler,
                                accepted_data_formats=["CI_TIF_DATA"])

    def copy(self):
        """Make a copy of the analysis.

        Returns
        -------
        analysis_copy
            Copy of the analysis.
        """
        analysis_copy = CicadaPyamnesiaFactorizationAnalysis(config_handler=self.config_handler)
        self.transfer_attributes_to_tabula_rasa_copy(analysis_copy=analysis_copy)
        return analysis_copy

    def check_data(self):
        """Check the data given and return ``True`` if the data given allows
        the analysis implemented, ``False`` otherwise.

        Returns
        -------
        bool
            If the data given allows the analysis implemented.
        """
        super().check_data()

        for session_index, session_data in enumerate(self._data_to_analyse):
            if session_data.DATA_FORMAT != "CI_TIF_DATA":
                self.invalid_data_help = f"Non CI_TIF_DATA format compatibility not yet implemented: " \
                                         f"{session_data.DATA_FORMAT}"
                return False

        return True

    def set_arguments_for_gui(self):
        """Add the CICADA arguments for the factorization analysis."""
        CicadaAnalysis.set_arguments_for_gui(self)

        self.add_open_dir_dialog_arg_for_gui(
            arg_name="path_to_skeleton_data",
            mandatory=True,
            short_description="Skeleton session folder",
            long_description="Folder of the previous skeleton session (should be similar to 'Skeleton_2020_01_01.00-00-00'). Must contain the skeleton analysis of each .tif input (each session); if one analysis is missing, the corresponding session will be skipped and a warning will be displayed.",
            family_widget="skeleton"
        )

        # ------ factorization ----- #
        self.add_choices_arg_for_gui(
            arg_name="factorization_method",
            choices=["ae", "pca", "nmf"],
            short_description="Method used to perform matrix factorization",
            long_description="Which method you want to use for the matrix factorization analysis.\nThe choices are:\n- nmf: perform a Non-negative Matrix Factorization (NMF) over the .tif sequence;\n- pca: perform a Principal Components Analysis (PCA) over the .tif sequence, followed by an Independent Components Analysis (ICA);\n- ae: perform a dimensionality reduction of the .tif sequence using an autoencoder, followed by an Independent Components Analysis (ICA).",
            default_value="nmf",
            multiple_choices=False,
            family_widget="factorization"
            )
        self.add_int_values_arg_for_gui(
            arg_name="nb_comp_intermed",
            min_value=1,
            max_value=1000,
            short_description="Number of components (intermediate)",
            long_description="When choosing the 'ae' or 'pca' factorization method, you ask pyAMNESIA to perform a dimensionality reduction and then a source separation. The intermediate number of components specifies the number of dimensions you want the output of the dimensionality reduction to have.",
            default_value=100,
            family_widget="factorization"
            )
        self.add_int_values_arg_for_gui(
            arg_name="nb_comp_final",
            min_value=1,
            max_value=1000,
            short_description="Number of components (final)",
            long_description="The final number of components specifies the number of components you want in the final output.",
            default_value=50,
            family_widget="factorization"
            )
        self.add_field_float_option_for_gui(
            arg_name="skewness_threshold",
            short_description="Skewness threshold",
            long_description="After having computed the final components, you can ask pyAMNESIA to check their skewness (measure of gaussianity). If the skewness of a component is too low, it means that it is noise and we remove it. A skewness threshold of `s_t` will remove all components of skewness inferior to `s_t`.",
            mandatory=False,
            default_value=0.08,
            family_widget="factorization"
            )


        self.add_image_format_package_for_gui()

        self.add_with_timestamp_in_filename_arg_for_gui()


    def update_original_data(self):
        """To be called if the data to analyse should be updated after the
        analysis has been run.

        Returns
        -------
        bool
            ``True`` if the data has been modified
        """
        pass

    def run_analysis(self, **kwargs):
        """Run the factorization analysis.

        Parameters
        ----------
        **kwargs : dict
            Config dictionary for factorization.
        """
        CicadaAnalysis.run_analysis(self, **kwargs)

        # format kwargs
        verbose = True
        config_misc, config_factorization = format_factorization_config(kwargs)
        config_misc['results_path'] = self.get_results_path()  # security

        # start runs
        n_sessions = len(self._data_to_analyse)
        plural = "s" if n_sessions >= 2 else ""
        print(f"{n_sessions} session{plural}")
        path_to_skeleton_data = kwargs['path_to_skeleton_data']

        for session_index, session_data in enumerate(self._data_to_analyse):
            session_identifier = session_data.identifier
            print(f"-------------- {session_identifier} -------------- ")

            # update results path
            config_misc['results_path'] = os.path.join(self.get_results_path(), session_identifier)

            # get skeleton results
            path_to_skeleton_data_session = os.path.join(path_to_skeleton_data, session_identifier)

            # check if files are present
            file_folders = ['sequence_projection', 'sequence_projection']
            file_names = ['projected_traces.npy', 'projected_coordinates.npy']
            files_are_present = True
            for file_folder, file_name in zip(file_folders, file_names):
                if not os.path.isfile(os.path.join(path_to_skeleton_data_session, file_folder, file_name)):
                    print(f"Unable to find a skeleton result ({file_name}).")
                    files_are_present = False
                    break
            if not files_are_present:
                print(f"Skipping {session_identifier}.\n")
                continue

            # load data
            projected_traces = np.load(os.path.join(path_to_skeleton_data_session, 'sequence_projection', 'projected_traces.npy'))
            projected_coordinates = np.load(os.path.join(path_to_skeleton_data_session, 'sequence_projection', 'projected_coordinates.npy'), allow_pickle=True)

            # factorization
            components = factorization(
                tif_sequence=session_data.get_tif_data(),
                traces=projected_traces,
                projected_coordinates=projected_coordinates,
                config_factorization=config_factorization,
                config_misc=config_misc,
            )

            if verbose:
                print(" ")
            self.update_progressbar(time_started=self.analysis_start_time, increment_value=100 / (n_sessions+1))

        print(f"Factorization analysis run in {time() - self.analysis_start_time:.2f} sec")

