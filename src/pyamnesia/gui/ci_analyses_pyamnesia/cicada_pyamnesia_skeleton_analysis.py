"""Skeletonization analysis for CICADA."""
from time import time
from cicada.analysis.cicada_analysis import CicadaAnalysis
# enabling pyAMNESIA imports from CICADA
import os
analyses_dir = os.path.dirname(__file__)
pyamnesia_dir = os.path.abspath(os.path.join(analyses_dir, '../../..'))  # src
import sys
sys.path.append(pyamnesia_dir)
# pyAMNESIA imports
from pyamnesia.gui.format_config import format_skeleton_config
from pyamnesia.signal_processing.sequence_processing import denoise_sequence
from pyamnesia.skeleton.skeletonize_sequence import skeletonize_sequence


class CicadaPyamnesiaSkeletonAnalysis(CicadaAnalysis):

    """Skeletonization analysis for CICADA.

    See also
    --------
    cicada.analysis.ci_analyses
    """


    def __init__(self, config_handler=None):
        """Class initialization."""
        CicadaAnalysis.__init__(self, name="Skeleton", family_id="pyAMNESIA",
                                short_description="Skeletonize a sequence",
                                long_description="The ``skeleton`` module focuses on the **morphological analysis** of the data. It also reduces the dimensions of it, which is very useful to reduce the computational time of the activity analysis that can be run behind it.\nFor more information, please visit pyAMNESIA documentation (https://pyamnesia.readthedocs.io/).",
                                config_handler=config_handler,
                                accepted_data_formats=["CI_TIF_DATA"])

    def copy(self):
        """Make a copy of the analysis.

        Returns
        -------
        analysis_copy
            Copy of the analysis.
        """
        analysis_copy = CicadaPyamnesiaSkeletonAnalysis(config_handler=self.config_handler)
        self.transfer_attributes_to_tabula_rasa_copy(analysis_copy=analysis_copy)
        return analysis_copy

    def check_data(self):
        """Check the data given and return ``True`` if the data given allows
        the analysis implemented, ``False`` otherwise.

        Returns
        -------
        bool
            If the data given allows the analysis implemented.
        """
        super().check_data()

        for session_index, session_data in enumerate(self._data_to_analyse):
            if session_data.DATA_FORMAT != "CI_TIF_DATA":
                self.invalid_data_help = f"Non CI_TIF_DATA format compatibility not yet implemented: " \
                                         f"{session_data.DATA_FORMAT}"
                return False

        return True

    def set_arguments_for_gui(self):
        """Add the CICADA arguments for the skeletonization analysis."""
        CicadaAnalysis.set_arguments_for_gui(self)

        # ------ denoising ----- #
        self.add_choices_arg_for_gui(
            arg_name="denoising_method",
            choices=["2D", "3D"],
            short_description="Method used to denoise the sequence",
            long_description="Method used to denoise the sequence.\nThe choices are: \n- 2D: Smooth each frame;\n- 3D: Smooth the whole sequence, as a 3D image.",
            default_value="3D",
            multiple_choices=False,
            family_widget="denoising"
            )
        self.add_field_float_option_for_gui(
            arg_name="sigma_denoising",
            short_description="Sigma for gaussian smoothing",
            long_description="Sigma in gaussian smoothing for denoising the sequence.",
            mandatory=True,
            default_value=1.0,
            family_widget="denoising"
            )
        # ------ summary ----- #
        self.add_choices_arg_for_gui(
            arg_name="summary_method",
            choices=["mean", "median", "standard_deviation"],
            short_description="Method used to generate the sequence summary image",
            long_description="The ``summary_method`` parameter selects which method to use for the generation of a summary image from the ``.tif`` movie. The choices are:\n- ``'mean'``: get the mean image along the time axis;\n- ``'median'``: get the median image along the time axis;\n- ``'standard_deviation'``: get the standard deviation image along the time axis.",
            default_value="mean",
            multiple_choices=False,
            family_widget="summarize"
            )
        self.add_int_values_arg_for_gui(
            arg_name="std_grouping_ratio",
            min_value=1,
            max_value=100,
            short_description="Grouping ratio for ``standard_deviation`` summary method",
            long_description="The ``std_grouping_ratio`` parameter selects the number of sequence frames to group and average before computing the sequence standard deviation in order to remove noise from the summary image. It should be large enough to remove the noise, but not too much, to preserve the intensity variations of the neurites.",
            default_value=10,
            family_widget="summarize"
            )
        # ------ skeletonize ----- #
        self.add_choices_arg_for_gui(
            arg_name="projection_substructure",
            choices=["skeleton", "active"],
            short_description="The spatial structure on which we project the sequence intensities",
            long_description="The ``projection_substructure`` parameter selects which structure is used for the mask on which we project the intensities.\n- ``'skeleton'``: the mask is the skeletonized image;\n- ``'active'``: the mask is the binary image, just before the step of skeletonization in Image skeletonization.",
            default_value="skeleton",
            multiple_choices=False,
            family_widget="skeletonize"
            )
        self.add_field_float_option_for_gui(
            arg_name="sigma_smoothing",
            short_description="Sigma for gaussian smoothing",
            long_description="The ``sigma_smoothing`` parameter selects the standard deviation for Gaussian kernel.\nThe higher it is, the smoother the image is, the fewer holes there are in the binary image.",
            mandatory=True,
            default_value=1.0,
            family_widget="skeletonize"
            )
        self.add_int_values_arg_for_gui(
            arg_name="adaptive_threshold_block_size",
            min_value=1,
            max_value=100,
            short_description="Adaptive threshold block size",
            long_description="The ``adaptive_threshold_block_size`` parameter selects the size of a pixel neighborhood that is used to calculate a threshold value for the pixel. It must be odd.\nShould be set so that the block captures a soma + a bit of its environment. For example, if in the movie, a typical soma size is 30x30, then ``43`` must be good value for ``adaptive_threshold_block_size`` to capture the environment.",
            default_value=43,
            family_widget="skeletonize"
            )
        self.add_int_values_arg_for_gui(
            arg_name="holes_max_size",
            min_value=1,
            max_value=100,
            short_description="Holes max size",
            long_description="The ``holes_max_size`` parameter selects the maximum size (number of pixels) of holes we want to fill in the binary image. Some parasitic holes may appear during binarization. Most of the time, they appear inside the somas or inside the branches, due to defects in thresholding. Caution: these parasitic holes must not be confused with larger holes, representing rings, or simply void between branches. A good advice to find the perfect value, is to identify the minimum size of the holes we want to keep (little rings for instance), and take a threshold just under.",
            default_value=50,
            family_widget="skeletonize"
            )
        self.add_int_values_arg_for_gui(
            arg_name="parasitic_skeletons_size",
            min_value=1,
            max_value=100,
            short_description="Parasitic skeletons size",
            long_description="The ``parasitic_skeletons_size`` parameter selects the maximum size (number of pixels) of isolated skeletons we want to remove during image skeletonization.",
            default_value=5,
            family_widget="skeletonize"
            )
        # ------ projection ----- #
        self.add_choices_arg_for_gui(
            arg_name="projection_method",
            choices=["all", "pixel", "branch"],
            short_description="Method used to project intensity",
            long_description="The ``projection_method`` parameter selects the projection method. For ``pixels``, we simply take the coordinates of the white pixels, and get their trace. For ``branch``, we first get the coordinates of all the pixels in the branches, using ``skan`` package, and then average their intensity, through time.",
            default_value="branch",
            multiple_choices=False,
            family_widget="projection"
            )
        self.add_int_values_arg_for_gui(
            arg_name="branch_radius",
            min_value=0,
            max_value=100,
            short_description="Branch radius",
            long_description="The ``branch_radius`` parameter selects the radius of the branches. In the case of ``pixel``, it is only for display, but in the case of ``branch``, all the pixels in the dilated branch are taken into account in the branch intensity averaging.",
            default_value=3,
            family_widget="projection"
            )
        self.add_choices_arg_for_gui(
            arg_name="element_normalization_method",
            choices=[None, "mean-substraction", "z-score"],
            short_description="Method used to normalize the traces",
            long_description="The ``element_normalization_method`` parameter selects the method for normalizing the traces. The choices are:\- ``'null'``: no normalization;\- ``'mean-substraction'``;\n- ``'z-score'``.\nThe common method for trace processing is ``z-score``, because it allows to set en the same footing all the elements we consider. But we can imagine some cases where this method is not suitable, for instance if we want to give more importance to the somatas, which have high intensities.",
            default_value="z-score",
            multiple_choices=False,
            family_widget="projection"
            )
        self.add_int_values_arg_for_gui(
            arg_name="element_smoothing_window_len",
            min_value=0,
            max_value=100,
            short_description="Smoothing window length for convolution (must be odd)",
            long_description="The ``element_smoothing_window_len`` parameter selects the dimension of the smoothing window used for convolution. It must be odd.\nSet it below the minimum frame difference between two transients. Otherwise, 2 transients would be merged !",
            default_value=11,
            family_widget="projection"
        )
        # ------ branch validation ----- #
        self.add_field_float_option_for_gui(
            arg_name="skewness_threshold",
            short_description="Skewness threshold (float!)",
            long_description="The ``skewness_threshold`` parameter selects the skewness value above which a branch isn't considered as valid.",
            mandatory=True,
            default_value=2.5,
            family_widget="validate_branches"
            )
        self.add_int_values_arg_for_gui(
            arg_name="transient_neighborhood",
            min_value=1,
            max_value=5,
            short_description="Transient neighborhood",
            long_description="The ``transient_neighborhood`` parameter selects the number of frames between transients onsets and peaks.",
            default_value=2,
            family_widget="validate_branches"
            )
        self.add_int_values_arg_for_gui(
            arg_name="pixels_around",
            min_value=0,
            max_value=20,
            short_description="Pixels around branch for display",
            long_description="The ``pixels_around`` parameter selects the number of pixels to display around the analyzed branch, for visualization.",
            default_value=5,
            family_widget="validate_branches"
            )
        # ------ peak properties ----- #
        self.add_field_float_option_for_gui(
            arg_name="height",
            short_description="Peak height",
            long_description="Required height of peaks.",
            mandatory=False,
            default_value=None,
            family_widget="peak_properties"
            )
        self.add_field_float_option_for_gui(
            arg_name="threshold",
            short_description="Peak threshold",
            long_description="Required threshold of peaks, the vertical distance to its neighboring samples.",
            mandatory=False,
            default_value=None,
            family_widget="peak_properties"
            )
        self.add_field_float_option_for_gui(
            arg_name="distance",
            short_description="Peak distance",
            long_description="Required minimal horizontal distance in samples between neighbouring peaks.",
            mandatory=False,
            default_value=None,
            family_widget="peak_properties"
            )
        self.add_field_float_option_for_gui(
            arg_name="prominence",
            short_description="Peak prominence",
            long_description="Required prominence of peaks.",
            mandatory=False,
            default_value=None,
            family_widget="peak_properties"
            )
        self.add_field_float_option_for_gui(
            arg_name="width",
            short_description="Peak width",
            long_description="Required width of peaks in samples.",
            mandatory=False,
            default_value=None,
            family_widget="peak_properties"
            )
        # ------ save denoised sequence ----- #
        self.add_bool_option_for_gui(
            arg_name="save_denoised_sequence",
            true_by_default=False,
            short_description="Save denoised sequence (preprocessing)",
            long_description="Save the denoised sequence. Only useful for visualization. Due to the important size of the data (> 500 Mo), we advise do set this parameter to ``'False'`` when not needed.",
            family_widget="image_format"
        )
        # ------ save projected sequence ----- #
        self.add_bool_option_for_gui(
            arg_name="save_projected_sequence",
            true_by_default=False,
            short_description="Save sequence projected on skeleton",
            long_description="Save the sequence projected on the skeleton or the active mask. Only useful for visualization. Due to the important size of the data (> 500 Mo), we advise do set this parameter to ``'False'`` when not needed.",
            family_widget="image_format"
            )
        # ------ save branch dataframe ----- #
        self.add_bool_option_for_gui(
            arg_name="save_branch_dataframe",
            true_by_default=False,
            short_description="Save branch DataFrame",
            long_description="Save the branch DataFrame generated by Skan module. Only useful for further skeleton morphological study. We advise do set this parameter to ``'False'`` when not needed.",
            family_widget="image_format"
        )


        self.add_image_format_package_for_gui()

        self.add_with_timestamp_in_filename_arg_for_gui()


    def update_original_data(self):
        """To be called if the data to analyse should be updated after the
        analysis has been run.

        Returns
        -------
        bool
            ``True`` if the data has been modified
        """
        pass

    def run_analysis(self, **kwargs):
        """Run the skeletonization analysis.

        Parameters
        ----------
        **kwargs : dict
            Config dictionary for skeletonization.
        """
        CicadaAnalysis.run_analysis(self, **kwargs)

        # format kwargs
        verbose = True
        config_misc, config_denoising, config_skeleton = format_skeleton_config(kwargs)
        config_misc['results_path'] = self.get_results_path()  # security

        # start runs
        n_sessions = len(self._data_to_analyse)
        plural = "s" if n_sessions >= 2 else ""
        print(f"{n_sessions} session{plural}")

        for session_index, session_data in enumerate(self._data_to_analyse):
            session_identifier = session_data.identifier
            print(f"-------------- {session_identifier} -------------- ")

            # update results path
            config_misc['results_path'] = os.path.join(self.get_results_path(), session_identifier)

            # denoise sequence
            denoised_tif = denoise_sequence(session_data.get_tif_data(), config_denoising, config_misc)
            # skeletonize
            projected_coordinates, projected_traces, projected_mask = skeletonize_sequence(denoised_tif, config_skeleton, config_misc)

            if verbose:
                print(" ")
            self.update_progressbar(time_started=self.analysis_start_time, increment_value=100 / (n_sessions+1))

        print(f"Skeleton analysis run in {time() - self.analysis_start_time:.2f} sec")

