"""Allow conversion between config dictionaries of CICADA and pyAMNESIA."""
import os
import yaml
import json


# ----- general functions ----- #

def _empty_config():
    """Return an empty config dict.

    Loads the config.yaml file and removes all the values.

    Returns
    -------
    config_dict : dict
        Empty config dictionary.
    """
    gui_dir = os.path.dirname(__file__)
    config_dir = os.path.abspath(os.path.join(gui_dir, '../config.yaml'))
    with open(config_dir) as f:
        config_dict = yaml.load(f, Loader=yaml.FullLoader)
    config_dict = _empty_dict(config_dict)
    return config_dict


def _empty_dict(config_dict):
    """Simple recursive function that empties a dictionary.

    Parameters
    ----------
    config_dict :
        Config dictionary to empty.

    Returns
    -------
    dict
        Empty dictionary.
    """
    for key, value in config_dict.items():
        if isinstance(value, dict):
            config_dict[key] = _empty_dict(value)
        else:
            config_dict[key] = None
    return config_dict


def _affect(config_dict, flat_key, flat_value):
    """Recursively affect a value to a dict.

    Parameters
    ----------
    config_dict : dict
        Config dictionary.
    flat_key : str
        Key we're looking for.
    flat_value :
        Value to affect to **flat_key**.
    """
    for key, value in config_dict.items():
        if isinstance(value, dict):
            _affect(value, flat_key, flat_value)

        if key == flat_key:  # found key
            config_dict[key] = flat_value


# ----- skeleton functions ----- #

def format_skeleton_config(flat_dict):
    """Format the skeletonization config dictionary.

    Parameters
    ----------
    flat_dict :
        Flat dictionary, from CICADA GUI.

    Returns
    -------
    config_misc, config_denoising, config_skeleton : (dict, dict, dict)
        Three config dictionaries needed for skeletonization.
    """
    config_dict = _empty_config()
    for flat_key, flat_value in flat_dict.items():
        if flat_value == 'None':  # in case bad conversion happened before
            flat_value = None
        _affect(config_dict, flat_key, flat_value)

    return config_dict['misc'], config_dict['denoising'], config_dict['skeleton']


# ----- clustering functions ----- #

def format_clustering_config(flat_dict):
    """Format the clustering config dictionary.

    Parameters
    ----------
    flat_dict :
        Flat dictionary, from CICADA GUI.

    Returns
    -------
    config_misc, config_clustering : (dict, dict)
        Two config dictionaries needed for clustering.
    """
    config_dict = _empty_config()
    for flat_key, flat_value in flat_dict.items():
        if flat_value == 'None':  # in case bad conversion happened before
            flat_value = None
        _affect(config_dict, flat_key, flat_value)

    return config_dict['misc'], config_dict['clustering']


# ----- factorization functions ----- #

def format_factorization_config(flat_dict):
    """Format the factorization config dictionary.

    Parameters
    ----------
    flat_dict :
        Flat dictionary, from CICADA GUI.

    Returns
    -------
    config_misc, config_factorization : (dict, dict)
        Three config dictionaries needed for factorization.
    """
    config_dict = _empty_config()
    for flat_key, flat_value in flat_dict.items():
        if flat_value == 'None':  # in case bad conversion happened before
            flat_value = None
        _affect(config_dict, flat_key, flat_value)

    return config_dict['misc'], config_dict['factorization']
