"""``.tif`` wrapper for CICADA."""
import random
from cicada.analysis.cicada_analysis_format_wrapper import CicadaAnalysisFormatWrapper
# enabling pyAMNESIA imports from CICADA
import os
gui_dir = os.path.dirname(__file__)
pyamnesia_dir = os.path.abspath(os.path.join(gui_dir, '../..'))  # src
import sys
sys.path.append(pyamnesia_dir)
# pyAMNESIA imports
from pyamnesia.utils import read_tif


class CicadaAnalysisTifWrapper(CicadaAnalysisFormatWrapper):

    """``.tif`` wrapper for CICADA.

    Allows to communicate with the tif format.

    Attributes
    ----------
    DATA_FORMAT : str
    WRAPPER_ID : str
    """

    DATA_FORMAT = "CI_TIF_DATA"

    WRAPPER_ID = "TIF_CI"

    def __init__(self, data_ref, load_data=True):
        """Class initialization.

        Parameters
        ----------
        data_ref
        load_data : bool, optional
        """
        CicadaAnalysisFormatWrapper.__init__(self, data_ref=data_ref, data_format="tif", load_data=load_data)
        self._tif_data = None
        if self.load_data_at_init:
            self.load_data()

    @staticmethod
    def is_data_valid(data_ref):
        """Check if the data can be an input for this wrapper as `data_ref`.

        Parameters
        ----------
        data_ref

        Returns
        -------
        bool
            If the data is valid.
        """
        if not os.path.isfile(data_ref):
            return False

        if data_ref.endswith(".tif") or data_ref.endswith(".tiff"):
            return True

        return False

    def load_data(self):
        """Load the data."""
        CicadaAnalysisFormatWrapper.load_data(self)
        self._tif_data = read_tif(self._data_ref)

    def __str__(self):
        """Return a string representing the session. Here the name of the
        ``.tif`` file.

        Returns
        -------
        str
            Name of the ``.tif`` file.
        """
        return os.path.basename(os.path.normpath(self._data_ref))

    def get_tif_data(self):
        """Return a ``np.array`` of the ``.tif`` sequence, and ``None`` if it
        is not available.

        Returns
        -------
        _tif_data : np.array
            Array of the ``.tif`` sequence.
        """
        return self._tif_data

    @property
    def identifier(self):
        """Identifier of the session.

        Returns
        -------
        str
            Identifier of the session.
        """
        return os.path.splitext(os.path.basename(self._data_ref))[0]
