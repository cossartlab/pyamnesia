"""Perform matrix factorization."""
from pyamnesia.utils import vectors_to_matrices, save_data
from pyamnesia.correlation.factorization.modules.ica import autoencoder, pca, ica
from pyamnesia.correlation.factorization.modules.nmf import nmf
from pyamnesia.correlation.factorization.modules.process_components import process_components


def factorization(tif_sequence, traces, projected_coordinates, config_factorization, config_misc):
    """Perform matrix factorization.

    Implements NMF, PCA, ICA and autoencoders.
    If ICA is selected, performs a dimensionality reduction first using PCA or
    autoencoders. Then, selects the resulting components by their skewness.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    projected_coordinates : np.array
        Coordinates of the components of the entities. Used to unravel the
        vectors.
    config_factorization : dict
        Config dictionary for `factorization`.
        Contains **factorization_method**, that can be one of ``'ae'``,
        ``'pca'`` and ``'nmf'``, **nb_comp_intermed**, that is the number of
        components for the dimensionality reduction, **nb_comp_final**, that is
        the final number of components, and **skewness_threshold**, that is the
        skewness threshold for the components post-processing.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    components : np.array
        Array of final components.
    """
    method = config_factorization['factorization_method']
    nb_comp_intermed = config_factorization['nb_comp_intermed']
    nb_comp_final = config_factorization['nb_comp_final']
    if config_factorization['skewness_threshold'] is None:
        config_factorization['skewness_threshold'] = 0
    skewness_threshold = config_factorization['skewness_threshold']
    results_path = config_misc['results_path']
    height, width = tif_sequence.shape[1:]

    if method == 'ae':
        # if nb_comp_intermed != 50:  # get nb of comp of AE somewhere
        #     print("Warning")
        ae(vectors)
        ica(vectors)

    elif method == 'pca':
        reduced_traces = pca(traces, var_goal=nb_comp_intermed)
        components = ica(reduced_traces, nb_comp=nb_comp_final)

    elif method == 'nmf':
        frames = traces.T
        components = nmf(frames, nb_comp=nb_comp_final)

    # skewness
    components = process_components(tif_sequence, components, projected_coordinates, config_factorization, config_misc)

    # save components
    print("[INFO] Saving components...")
    components = vectors_to_matrices(components, height, width, unravel_list=projected_coordinates)
    for i_comp, component in enumerate(components):
        save_data(component, 'image_plt', results_path, 'components', f"comp_{i_comp + 1}.png")

    return components
