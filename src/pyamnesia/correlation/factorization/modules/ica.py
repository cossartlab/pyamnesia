"""Perform PCA, AE and ICA."""
from pyamnesia.utils import timed
from sklearn.decomposition import PCA, FastICA


# --- DIMENSIONALITY REDUCTION --- #


@timed
def pca(traces, var_goal):
    """Perform PCA dimensionality reduction on the sequence.

    Parameters
    ----------
    traces : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    var_goal : float, int
        Percentage of variance to keep. If ``int``, then number of components
        to have in the output.

    Returns
    -------
    reduced_traces : np.array
        Dimensionally reducted traces.

    See also
    --------
    sklearn.decomposition.PCA
    """
    nb_frames, nb_pixels = traces.shape

    pca = PCA(n_components=var_goal)
    print(f"Extracting top components for {var_goal} cumulated variance...")
    reduced_traces = pca.fit_transform(traces)
    print(f"I {traces.shape}\nO {reduced_traces.shape}")

    return reduced_traces


@timed
def autoencoder(traces, nb_comp):
    """Perform autoencoders dimensionality reduction on the sequence.

    Parameters
    ----------
    traces : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    nb_comp : int
        Number of components to have in the reducted traces.

    Returns
    -------
    reduced_traces : np.array
        Dimensionally reducted traces.

    Notes
    -----
    The function should create the model and load the weights.

    References
    ----------
    https://www.tensorflow.org/tutorials/keras/save_and_load
    """
    raise NotImplementedError("Autoencoders have not been implemented yet.")
    return None


# --- SOURCE SEPARATION --- #


@timed
def ica(reduced_traces, nb_comp):
    """Perform ICA on the reducted traces.

    Parameters
    ----------
    reduced_traces : np.array
        Dimensionally reducted traces.
    nb_comp : int
        Number of components to use. If none is passed, all are used.

    Returns
    -------
    components : np.array
        Output components of ICA.

    See also
    --------
    sklearn.decomposition.FastICA
    """
    if reduced_traces.shape[1] < nb_comp:
        print("Too many components asked for ICA. Using total number instead.")
        nb_comp = reduced_traces.shape[1]

    ica = FastICA(n_components=nb_comp, whiten=True)
    print(f"Extracting top {nb_comp} components...")
    ica.fit(reduced_traces.T)
    components = ica.components_
    print(f"I {reduced_traces.shape}\nO {components.shape}")

    return components
