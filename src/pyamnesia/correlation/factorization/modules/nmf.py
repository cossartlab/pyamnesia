"""Perform NMF on the sequence."""
from sklearn.decomposition import NMF, PCA
from pyamnesia.utils import timed, NotNonNegativeError


@timed
def nmf(frames, nb_comp, tol=1):
    """Perform NMF on the sequence.

    Parameters
    ----------
    frames : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    nb_comp : int
        Number of components, if **nb_comp** is not set all features are kept.
    tol : int, optional
        Tolerance of the stopping condition.

    Returns
    -------
    components : np.array
        Output components of NMF.

    See also
    --------
    sklearn.decomposition.NMF
    """
    if not (frames >= 0).all():
        raise NotNonNegativeError()

    if isinstance(nb_comp, float):  # if we specified a variance percentage
        print(f"Finding number of components for {nb_comp} variance...")
        pca = PCA(n_components=nb_comp)
        pca.fit(frames.T)  # fit with traces
        nb_comp = pca.n_components_
        print(f"-> {nb_comp} components.")

    print(f"Extracting {nb_comp} components...")
    nmf = NMF(n_components=nb_comp, random_state=0, tol=tol)
    nmf_output = nmf.fit_transform(frames)
    components = nmf.components_
    print("\tV\t=\tH\tx\tW")
    print(f"{frames.shape}\t= {nmf_output.shape}\tx {components.shape}")

    return components
