"""Apply post-processing to components."""
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import skew
from pyamnesia.utils import vectors_to_matrices, save_data, timed


@timed
def process_components(tif_sequence, components, projected_coordinates, config_factorization, config_misc):
    """
    Apply post-processing to components.

    Inverts components with a negative skewness, removes those with a skewness
    inferior to **skewness_threshold**, and clips the components to a minimum
    of 0.

    Parameters
    ----------
    tif_sequence : np.array
        ``.tif`` calcium imaging sequence that is being analyzed.
    components : np.array
        Array of flattened components before post-processing.
    projected_coordinates : np.array
        Coordinates of the components of the entities. Used to unravel the
        vectors.
    config_factorization : dict
        Config dictionary for `factorization`.
        Contains **factorization_method**, that can be one of 'ae', 'pca' and
        'nmf', **nb_comp_intermed**, that is the number of components for the
        dimensionality reduction, **nb_comp_final**, that is the final number
        of components, and **skewness_threshold**, that is the skewness
        threshold for the components post-processing.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    processed_components : np.array
        Processed components.

    Notes
    -----
    The components in input and outuput are flattened.
    """
    skewness_threshold = config_factorization['skewness_threshold']
    results_path = config_misc['results_path']
    height, width = tif_sequence.shape[1:]

    # inverse components with negative skewness
    indices_negative_skew = skew(components, axis=1) < 0
    components[indices_negative_skew] *= -1
    print(f"Inverted {np.count_nonzero(indices_negative_skew)}/{len(components)} components of negative skewness.")

    # plot histogram
    print("[INFO] Saving skewness histogram...")
    _plot_histogram(components, skewness_threshold, results_path)

    # plot components
    print("[INFO] Saving components skewness...")
    mat_components = vectors_to_matrices(components, height, width, unravel_list=projected_coordinates)
    _plot_components(components, mat_components, skewness_threshold, results_path)

    # select components
    comps_skew = skew(components, axis=1)
    skews_test = comps_skew >= skewness_threshold
    print(f"Removed {len(components) - np.count_nonzero(skews_test)}/{len(components)} components of skewness < {skewness_threshold}.")
    processed_components = components[skews_test].clip(min=0)

    return processed_components


def _plot_histogram(components, skewness_threshold, results_path):
    """Plot the skewness histogram of the components.

    Parameters
    ----------
    components :
        Array of flattened components that are being post-processed.
    skewness_threshold :
        Skewness threshold for the components post-processing. Components with
        a skewness inferior to **skewness_threshold** will be removed.
    results_path :
        Where the results are stored.
    """
    plt.ioff()
    fig, ax = plt.subplots(1, 1)
    y, _, _ = ax.hist(skew(components, axis=1), bins=len(components), edgecolor='white')
    ax.axvline(x=skewness_threshold, color='red', ls='--')
    ax.text(x=skewness_threshold, y=y.max(), s="threshold", color='red', backgroundcolor='white', horizontalalignment='center', bbox=dict(facecolor='white', edgecolor='none', alpha=0.7))
    ax.set_title('Skewness distribution')

    save_data(fig, 'figure', results_path, "skewness", "skewness_distribution.png")
    plt.close(fig)


def _plot_components(components, mat_components, skewness_threshold, results_path):
    """Plot the components with their skewness.

    If a component has a skewness that is inferior to **skewness_threshold**,
    it will have a red outline. Else, the outline will be green.

    Parameters
    ----------
    components :
        Array of flattened components that are being post-processed.
    mat_components :
        Array of unraveled components that are being post-processed.
    skewness_threshold :
        Skewness threshold for the components post-processing. Components with
        a skewness inferior to **skewness_threshold** will be removed.
    results_path :
        Where the results are stored.
    """
    nb_h, nb_w = (2, 4)
    nb_total = nb_h * nb_w
    nb_components = len(components)
    nb_figures = (nb_components - 1) // nb_total + 1

    # prepare skewnesses
    comps_skew = skew(components, axis=1)
    skews_test = comps_skew >= skewness_threshold

    # clip
    components = components.clip(min=0)
    mat_components = mat_components.clip(min=0)

    for id_figure in range(nb_figures):
        fig, axes = plt.subplots(nb_h, nb_w, figsize=(10, 5))

        # don't show empty axes
        for ax in axes.flatten():
            ax.axis('off')

        n_inf = nb_total * id_figure
        n_sup = nb_total * (id_figure + 1)

        # prepare plots
        vmin = np.min(components[n_inf:n_sup])  # 0
        vmax = np.max(components[n_inf:n_sup])

        zipped_components = zip(components[n_inf:n_sup], mat_components[n_inf:n_sup])
        fig.suptitle(f"Components (fig. {id_figure + 1}: {n_inf + 1} to {n_inf + len(components[n_inf:n_sup])} /{nb_components})")

        for index, (component, mat_component) in enumerate(zipped_components):
            # plot
            ax = axes[index // nb_w, index % nb_w]
            ax.axis('on')
            im = ax.imshow(mat_component, cmap='gray', vmin=vmin, vmax=vmax)
            comp_skew = comps_skew[n_inf + index]
            ax.set_title(f"C{n_inf + index + 1} - skew: {comp_skew:.2f}")
            ax.xaxis.set_visible(False)
            ax.yaxis.set_visible(False)

            # color
            skew_test = skews_test[n_inf + index]
            color = 'green' if skew_test else 'red'
            # ax.title.set_color(color)
            for loc in ['bottom', 'top', 'right', 'left']:  # border of ax
                ax.spines[loc].set_color(color)
        cbar = fig.colorbar(im, ax=axes.ravel().tolist())
        cbar.formatter.set_powerlimits((0, 0))
        cbar.update_ticks()

        save_data(fig, 'figure', results_path, "skewness", f"comp_{n_inf + 1}_to_{n_inf + len(components[n_inf:n_sup])}.png")
        plt.close(fig)
