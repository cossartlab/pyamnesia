"""Perfom trace clustering based on a succession of t-SNE projection and
HDBSCAN clustering."""
from pyamnesia.correlation.clustering.modules.dimensionality_reduction import dimensionality_reduction
from pyamnesia.correlation.clustering.modules.clustering_methods import hdbscan_clustering
from pyamnesia.correlation.clustering.modules.clusters_analysis import analyse_clusters
from pyamnesia.utils import save_data


def clustering(projected_traces, projected_coordinates, summary_image, projected_mask, config_clustering, config_misc):
    """Perfom trace clustering based on a succession of t-SNE projection and
    HDBSCAN clustering.

    Parameters
    ----------
    projected_traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    projected_coordinates : np.array
        Coordinates of the components of the entities. Used to unravel the
        vectors.
    summary_image : np.array
        Image that summarizes the ``.tif`` sequence, following the
        **summarize_sequence** method.
    projected_mask : np.array
        Binary image of the skeleton mask.
    config_clustering : dict
        Config dictionary for `clustering`.
        Contains the sub config dictionaries **config_dimensionality_reduction**
        and **config_hdbscan_clustering**.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    clusters_masks : np.array
        Binary masks of the clusters on **tif_sequence**.
    clusters_coordinates : np.array
        Coordinates of the components of the clusters. Used to unravel the
        vectors.
    clusters_mean_traces : np.array
        Mean traces of the clusters.
    """
    results_path = config_misc['results_path']

    # Project traces in a 2D plane with TSNE
    low_dim_traces = dimensionality_reduction(projected_traces,
                                                config_clustering['dimensionality_reduction'],
                                                config_misc)

    # Find clusters among the traces
    clusters_labels = hdbscan_clustering(low_dim_traces,
                                         config_clustering['hdbscan_clustering'],
                                         config_misc)

    # Analyse clusters
    clusters_masks, clusters_coordinates, clusters_mean_traces = analyse_clusters(clusters_labels, projected_coordinates, projected_traces, summary_image, projected_mask, config_misc)

    # Save clusters coordinates and mean traces in a ``.npy`` format
    print("[INFO] Saving components...")
    save_data(clusters_coordinates, 'npy', results_path, "sequence_projection", "projected_coordinates.npy")
    save_data(clusters_mean_traces, 'npy', results_path, "sequence_projection", "projected_traces.npy")
    suite_2p_coordinates = [{'ypix': clusters_coordinates[idx][:, 0], 'xpix': clusters_coordinates[idx][:, 1]} for idx in range(len(clusters_coordinates))]
    save_data(suite_2p_coordinates, 'npy', results_path, "sequence_projection", "Add_suite2p_projected_coordinates.npy")
    return clusters_masks, clusters_coordinates, clusters_mean_traces
