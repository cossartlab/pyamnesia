"""Perform dimensionality reduction of the traces, using temporal PCA and t-SNE."""
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.metrics import pairwise_distances
import sklearn.metrics.pairwise as smp
from sklearn.manifold import TSNE
from pyamnesia.utils import timed, save_data
from pyamnesia.correlation.clustering.modules import distance_metrics
from pyamnesia.signal_processing.trace_processing import normalize_traces


def temporal_pca(traces, normalization_method, variance_goal, config_misc):
    """Perform temporal PCA of the traces.

    The reduced dimension depends on `variance_goal`.

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    normalization_method : str
        The method for normalizing the traces. It can be one of ``'null'``,
        ``'mean-substraction'`` and ``'z-score'``.
    variance_goal : float
        Percentage of variance to keep after PCA. Has to be inferior to ``1``.
        The higher it is, the more we keep principal components.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    reduced_traces : np.array
        Traces of the entities (pixels, branches...) to analyze. Projected in a
        lower dimensional space after PCA.
    """
    results_path = config_misc['results_path']
    traces = normalize_traces(traces, normalization_method=normalization_method)
    pca = PCA(n_components=variance_goal)
    reduced_traces = pca.fit_transform(traces)

    traces_proj = pca.inverse_transform(reduced_traces)
    for index, (trace, reduced_trace) in enumerate(zip(traces[:10], traces_proj[:10])):
        plt.ioff()
        fig, ax = plt.subplots(1, 1)
        ax.plot(trace)
        ax.plot(reduced_trace)
        ax.set_title(f'Before PCA : {traces.shape[1]}\n'
                     f'After PCA : {reduced_traces.shape[1]}')
        save_data(fig, 'figure', results_path, f"dimensionality_reduction", f"pca_res_{index}.png")
        plt.close(fig)

    return reduced_traces


def get_distance_matrix(traces, distance_metric):
    """Get the distance matrix of the traces, following the **distance_metric**.

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    distance_metric : str
        The distance metric for t-SNE distance matrix computation.
        It can be one of ``pearson``, ``spearman`` and every metric in
        ``sklearn.metrics.pairwise.distance_metrics``.

    Returns
    -------
    distance_matrix : np.array
        The matrix of distances between the data points.

    Raises
    ------
    NotImplementedError
        When a method name isn't related to any implemented method.

    References
    ----------
    - https://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise.distance_metrics.html
    """
    if distance_metric in smp.distance_metrics():
        distance_matrix = pairwise_distances(traces, metric=distance_metric)
    else:
        try:
            # get function from the metrics module
            distance_metric_fun = getattr(distance_metrics, distance_metric)
            distance_matrix = distance_metric_fun(traces)
        except AttributeError:
            raise NotImplementedError(f"The {distance_metric} metric has not been implemented.")
    return distance_matrix


def tsne_projection(distance_matrix, config_dimensionality_reduction):
    """Perform t-SNE on the distance matrix, to get the 2D projected traces.

    Parameters
    ----------
    distance_matrix : np.array
        The matrix of distances between the data points.
    config_dimensionality_reduction : dict
        Config dictionary for `dimensionality_reduction`.
        Contains **normalization_method**, **pca_variance_goal**,
        **tsne_distance_metric**, **tsne_perplexity** and **tsne_random_state**.

    Returns
    -------
    low_dim_traces : np.array
        Traces of the entities (pixels, branches...) to analyze. Projected in a
        2D plane with t-SNE.

    References
    ----------
    - ``sklearn.manifold.TSNE``: https://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html
    """
    perplexity = config_dimensionality_reduction['tsne_perplexity']
    random_state = config_dimensionality_reduction['tsne_random_state']

    tsne = TSNE(metric='precomputed',
                perplexity=perplexity,
                random_state=random_state)
    low_dim_traces = tsne.fit_transform(distance_matrix)

    return low_dim_traces


@timed
def dimensionality_reduction(traces, config_dimensionality_reduction, config_misc):
    """Perform dimensionality reduction of the traces, using temporal PCA and t-SNE.

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    config_dimensionality_reduction : dict
        Config dictionary for `dimensionality_reduction`.
        Contains **normalization_method**, **pca_variance_goal**,
        **tsne_distance_metric**, **tsne_perplexity** and **tsne_random_state**.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    low_dim_traces : np.array
        Traces of the entities (pixels, branches...) to analyze. Projected in a
        2D plane with t-SNE.
    """
    variance_goal = config_dimensionality_reduction['pca_variance_goal']
    distance_metric = config_dimensionality_reduction['tsne_distance_metric']
    normalization_method = config_dimensionality_reduction['normalization_method']

    reduced_traces = temporal_pca(traces, normalization_method=normalization_method, variance_goal=variance_goal, config_misc=config_misc)
    distance_matrix = get_distance_matrix(reduced_traces, distance_metric=distance_metric)
    low_dim_traces = tsne_projection(distance_matrix,
                                       config_dimensionality_reduction)

    return low_dim_traces
