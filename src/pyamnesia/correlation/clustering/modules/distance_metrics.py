"""Perform distance matrix computation, based on a specific metric."""
import numpy as np
from sklearn.metrics import pairwise_distances
from pyamnesia.signal_processing.trace_processing import rank_traces
from pyamnesia.signal_processing.transient_analysis import get_transients_traces


def pearson(traces):
    """Perform distance matrix computation, using Pearson correlation metric.

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.

    Returns
    -------
    distance_matrix : np.array
        The matrix of distances between the data points, using Pearson
        correlation metric.
    """
    # Compute the pearson correlation matrix
    pearson_correlation_matrix = np.corrcoef(traces)

    # Convert it to a distance matrix
    distance_matrix = 1 - pearson_correlation_matrix
    return distance_matrix


def spearman(traces):
    """Perform distance matrix computation, using Spearman correlation metric.

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.

    Returns
    -------
    distance_matrix : np.array
        The matrix of distances between the data points, using Spearman
        correlation metric.
    """
    # Compute the spearman correlation matrix,
    # which is also the pearson correlation matrix of the ranked traces
    ranked_traces = rank_traces(traces)
    spearman_correlation_matrix = np.corrcoef(ranked_traces)

    # Convert it to a distance matrix
    distance_matrix = 1 - spearman_correlation_matrix
    return distance_matrix


def transient(traces, peak_properties, transients_values, binning_radius, distance_metric):
    """Perform distance matrix computation, using **transient** correlation metric.

    It consists in getting the **transients_traces** from the transients, as
    explained in the ``signal_processing`` module, and computing the distance
    between the arrays, using the distance metric **distance_metric**.

    Parameters
    ----------
    traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    peak_properties : dict, optional
        Dictionary to parametrize the traces peaks detection.
        Contains **height**, **threshold**, **distance**, **prominence** and
        **width**.
    transients_values : str
        The values of transients used in the `trace to transient` transform.
        Can be one of **constant**, **height** and **prominence**.
    binning_radius : int
        The standard deviation used during transients convolution.
        Reduces the locality of the transients, and thus makes traces with
        close transients more correlated.
    distance_metric : str
        The distance metric for t-SNE distance matrix computation, between the
        `traces to transients` arrays. It can be part of
        ``sklearn.metrics.pairwise.distance_metrics``.

    Returns
    -------
    distance_matrix : np.array
        The matrix of distances between the data points, using "transients" correlation metric.

    References
    ----------
    - ``scipy.signal.find_peaks``
    - https://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise.distance_metrics.html for more details.
    """
    transients_traces = get_transients_traces(traces, peak_properties=peak_properties,
                                              transients_values=transients_values,
                                              binning_radius=binning_radius)
    distance_matrix = pairwise_distances(transients_traces, metric=distance_metric)
    return distance_matrix
