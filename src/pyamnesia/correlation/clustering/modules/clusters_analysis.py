"""Perform clusters analysis."""
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import cv2
from pyamnesia.utils import timed, save_data
from pyamnesia.correlation.clustering.modules.distance_metrics import spearman


def get_clusters_infos(clusters_labels, projected_coordinates, projected_traces):
    """Get clusters coordinates, clusters traces, and clusters mean traces from
    the HDBSCAN clustering results.

    Parameters
    ----------
    clusters_labels : np.array
        An array of labels, relative to the clusters to which belong the input
        points in the data. ``-1`` stands for the points to which no cluster
        was attributed.
    projected_coordinates : np.array
        Coordinates of the components of the entities. Used to unravel the
        vectors.
    projected_traces : np.array
        Traces of the entities (pixels, branches...) to analyze.

    Returns
    -------
    clusters_coordinates : np.array
        Coordinates of the components of the clusters. Used to unravel the
        vectors.
    clusters_traces : np.array
        Traces of all the components of the clusters.
    clusters_mean_traces : np.array
        Mean traces of the clusters.
    """

    # remove the ``-1`` label
    clusters_coordinates = [[] for _ in range(0, np.max(clusters_labels)+1)]
    clusters_traces = [[] for _ in range(0, np.max(clusters_labels)+1)]
    clusters_mean_traces = [[] for _ in range(0, np.max(clusters_labels)+1)]

    # Add traces to their corresponding clusters
    for skeleton_element_coordinates, skeleton_element_trace, cluster_label in zip(projected_coordinates, projected_traces, clusters_labels):
        for coordinates in skeleton_element_coordinates:
            clusters_coordinates[cluster_label].append(coordinates)
        clusters_traces[cluster_label].append(skeleton_element_trace)

    # Compute the mean trace for each cluster
    for i in range(len(clusters_coordinates)):
        clusters_coordinates[i] = np.array(clusters_coordinates[i])
        clusters_traces[i] = np.array(clusters_traces[i])
        clusters_mean_traces[i] = clusters_traces[i].mean(axis=0)

    clusters_coordinates = np.array(clusters_coordinates, dtype=object)
    clusters_traces = np.array(clusters_traces, dtype=object)
    clusters_mean_traces = np.array(clusters_mean_traces)

    return clusters_coordinates, clusters_traces, clusters_mean_traces


def compute_intra_cluster_correlations(clusters_traces):
    intra_cluster_correlations = []
    for cluster_traces in clusters_traces:
        spearman_distance_matrix = spearman(cluster_traces)
        spearman_correlation_matrix = 1 - spearman_distance_matrix
        intra_cluster_correlation = spearman_correlation_matrix[np.triu_indices_from(spearman_correlation_matrix, 1)].mean()
        intra_cluster_correlations.append(intra_cluster_correlation)

    return np.array(intra_cluster_correlations)


def get_clusters_masks(clusters_coordinates, mask):
    """Get the clusters masks on the original sequence.

    Parameters
    ----------
    clusters_coordinates : np.array
        Coordinates of the components of the clusters. Used to unravel the
        vectors.
    mask : np.array
        Binary image of the skeleton mask.

    Returns
    -------
    clusters_masks : np.array
        Binary masks of the clusters on `tif_sequence`.
    """
    clusters_masks = []
    for cluster_coordinates in clusters_coordinates:
        cluster_mask = np.zeros_like(mask)
        for i, j in cluster_coordinates:
            cluster_mask[i, j] = 1
        clusters_masks.append(cluster_mask)
    return np.array(clusters_masks)


def plot_clusters_overlays(clusters_masks, summary_image, projected_mask, clusters_traces, intra_cluster_correlations,
                           config_misc):
    """Successively plot the clusters overlays on the summary image.

    Parameters
    ----------
    clusters_masks : np.array
        Binary masks of the clusters on `tif_sequence`.
    summary_image : np.array
        Image that summarizes the ``.tif`` sequence, following the
        **summarize_sequence** method.
    projected_mask : np.array
        Binary image of the skeleton mask.
    clusters_traces : np.array
        Traces of the clusters' components.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.
    """
    results_path = config_misc['results_path']

    for index, (cluster_mask, cluster_traces, intra_cluster_correlation) in enumerate(zip(clusters_masks,
                                                                                          clusters_traces,
                                                                                          intra_cluster_correlations)):
        plt.ioff()
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(15, 10))
        ax1.imshow(summary_image, cmap='gray')

        # Dilate the mask overlay and the cluster overlay for visualization
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
        projected_mask_dilated = cv2.dilate(projected_mask, kernel)
        cluster_mask_dilated = cv2.dilate(cluster_mask, kernel)

        projected_mask_overlay = np.ma.masked_where(projected_mask_dilated == 0, projected_mask_dilated)  # for overlay
        cluster_mask_overlay = np.ma.masked_where(cluster_mask_dilated == 0, cluster_mask_dilated)  # for overlay

        ax1.imshow(projected_mask_overlay, cmap='summer', alpha=0.5)
        ax1.imshow(cluster_mask_overlay, cmap='rainbow', alpha=0.8)
        ax1.set_title(f"Cluster mask")
        for cluster_trace in cluster_traces:
            ax2.plot(cluster_trace)

        ax2.set_title(f"Cluster traces (raw); Spearman Correlation = {intra_cluster_correlation:.2f}")
        fig.suptitle(f"Cluster n°{index}")
        save_data(fig, 'figure', results_path, "hdbscan_clustering", f"cluster_{index}.png")
        plt.close(fig)


def analyse_clusters(clusters_labels, projected_coordinates, projected_traces, summary_image, projected_mask, config_misc):
    """Perform clusters analysis.

    Parameters
    ----------
    clusters_labels : np.array
        An array of labels, relative to the clusters to which belong the input
        points in the data. ``-1`` stands for the points to which no cluster
        was attributed.
    projected_coordinates : np.array
        Coordinates of the components of the entities. Used to unravel the
        vectors.
    projected_traces : np.array
        Traces of the entities (pixels, branches...) to analyze.
    summary_image : np.array
        Image that summarizes the ``.tif`` sequence, following the
        **summarize_sequence** method.
    projected_mask : np.array
        Binary image of the skeleton mask.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    clusters_masks : np.array
        Binary masks of the clusters on `tif_sequence`.
    clusters_coordinates : np.array
        Coordinates of the components of the clusters. Used to unravel the
        vectors.
    clusters_mean_traces : np.array
        Mean traces of the clusters.
    """
    # Get clusters coordinates, traces and mean trace
    clusters_coordinates, clusters_traces, clusters_mean_traces = get_clusters_infos(clusters_labels, projected_coordinates, projected_traces)

    # Compute intra-cluster correlation
    intra_cluster_correlations = compute_intra_cluster_correlations(clusters_traces)

    # Get clusters masks
    clusters_masks = get_clusters_masks(clusters_coordinates, projected_mask)

    # Plot clusters overlays
    print("[INFO] Saving clusters overlays...")
    plot_clusters_overlays(clusters_masks, summary_image, projected_mask, clusters_traces, intra_cluster_correlations,
                           config_misc)

    # Compute inter-cluster correlation
    inter_cluster_correlations_matrix = 1 - spearman(clusters_mean_traces)
    fig, (ax1) = plt.subplots(1, 1, figsize=(15, 10))
    ax1.imshow(inter_cluster_correlations_matrix, cmap="plasma")
    save_data(fig, 'figure', config_misc['results_path'], "hdbscan_clustering", "correlation_matrix")

    return clusters_masks, clusters_coordinates, clusters_mean_traces
