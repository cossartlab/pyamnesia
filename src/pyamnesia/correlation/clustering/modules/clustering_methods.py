"""Perform clustering of 2D points based on HDBSCAN method."""
import os
import matplotlib.pyplot as plt
import numpy as np
import hdbscan
from pyamnesia.utils import timed, save_data, NoClusterError


@timed
def hdbscan_clustering(low_dim_traces, config_hdbscan, config_misc):
    """Perform clustering of 2D points based on HDBSCAN method.

    Parameters
    ----------
    low_dim_traces : np.array
        Traces of the entities (pixels, branches...) to analyze. Projected in a
        2D plane with t-SNE.
    config_hdbscan : dict
        Config dictionary for `hdbscan`.
        Contains **min_cluster_size**, **min_samples** and **hdbscan_metric**.
    config_misc : dict
        Config dictionary for `misc`.
        Contains **results_path**, where the results are stored,
        **save_denoised_sequence** and **save_projected_sequence**, that decide
        if the denoised and the projected sequence will be saved or not.

    Returns
    -------
    labels : np.array
        An array of labels, relative to the clusters to which belong the input
        points in the data. ``-1`` stands for the points to which no cluster
        was attributed.

    Raises
    ------
    NoClusterError
        When all the points are attributed ``-1``. In this case they are all
        isolated and we cannot process them further.

    References
    ----------
    - hdbscan.HDBSCAN: https://hdbscan.readthedocs.io/en/latest/api.html
    """
    min_cluster_size = config_hdbscan['min_cluster_size']
    min_samples = config_hdbscan['min_samples']
    metric = config_hdbscan['hdbscan_metric']
    results_path = config_misc['results_path']

    # Find clusters among the traces
    clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size,
                                min_samples=min_samples,
                                metric=metric)
    clusterer.fit(low_dim_traces)

    # Plot the 2D traces with their labels
    plt.ioff()
    fig, ax = plt.subplots(1, 1)
    for g in np.unique(clusterer.labels_):
        ix = np.where(clusterer.labels_ == g)
        ax.scatter(*low_dim_traces[ix].T, label=g, alpha=.6)
    ax.legend()
    ax.grid(True)
    fig.suptitle(f"{np.max(clusterer.labels_) + 1} clusters")
    print("[INFO] Saving clustering scatter plot...")
    save_data(fig, 'figure', results_path, f"hdbscan_clustering",
              f"hdbscan_mcs{min_cluster_size}_ms{min_samples}_{metric}.png")
    plt.close(fig)

    if np.max(clusterer.labels_) == -1:
        raise NoClusterError()

    return clusterer.labels_
