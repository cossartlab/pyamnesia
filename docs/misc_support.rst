=======
Support
=======

Contributing
============

You can read the contributing guidelines here_.

.. _here: https://gitlab.com/cossartlab/pyamnesia/-/blob/main/CONTRIBUTING.md

License
=======

.. include:: ../LICENSE

Contact
=======

If you have any question concerning the tool, feel free to contact us:

Cossart interns
    - Théo Dumont: *theo.dumont[at]mines-paristech.fr*
    - Tom Szwagier: *tom.szwagier[at]mines-paristech.fr*

Cossart team
    - Robin Dard: *robin.dard[at]inserm.fr*

