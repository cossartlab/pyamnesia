=======================
Configuration templates
=======================

Here we present some **configuration templates** that we used during our experiments. Used on the ``.tif`` sequence above, they produce good results. In order for you to see what we call "good" results, they are shown above too for each configuration template.

.. image:: /img/overview/introduction/calcium_imaging.gif
    :alt: Initial sequence
    :width: 300

*The initial sequence.*

The configuration templates can be found in the ``pyamnesia/config_templates/`` directory.

-----

Clustering templates
====================

Skeleton pixels clustering
--------------------------

- **file:** ``config_clustering_pixel.yaml``
- **context:** clustering the traces of skeleton pixels from the ``.tif`` sequence.
- **specs:** ``projection_substructure`` is set to ``'skeleton'`` and ``projection_method`` to ``'pixel'``. t-SNE perplexity is relatively high, because we expect to find mesoscopic clusters composed of dozens of pixels.
- **results:**

.. image:: img/misc/config/clustering_pixel.png
    :alt: Clustering plot, cluster overlay and cluster traces

*Clustering plot, cluster overlay and cluster traces.*

Skeleton branches clustering
----------------------------

- **file:** ``config_clustering_branch.yaml``
- **context:** clustering the traces of skeleton branches (or mesoscopic clusters from the pixel clustering) from the ``.tif`` sequence.
- **specs:** ``projection_substructure`` is set to ``'skeleton'`` and ``projection_method`` to ``'branch'``. t-SNE perplexity is lower, because we expect to find macroscopic clusters composed of a few branches (or mesoscopic clusters).
- **results:**

.. image:: img/misc/config/clustering_branch.png
    :alt: Clustering plot, cluster overlay and cluster traces

*Clustering plot, cluster overlay and cluster traces.*

Active pixels clustering
------------------------

- **file:** ``config_clustering_active.yaml``
- **context:** clustering the traces of active pixels from the ``.tif`` sequence.
- **specs:** ``projection_substructure`` is set to ``'active'`` and ``projection_method`` to ``'pixel'``. t-SNE perplexity is lower, because we expect to find macroscopic clusters composed of a few branches (or mesoscopic clusters).
- **results:**

.. image:: img/misc/config/clustering_active.png
    :alt: Clustering plot, cluster overlay and cluster traces

*Clustering plot, cluster overlay and cluster traces.*

-----

Factorization templates
=======================

NMF
---

- **file:** ``config_factorization_nmf.yaml``
- **context:** performing a NMF on the ``.tif`` sequence.
- **specs:** ``element_normalization_method`` is set to ``null`` so that the input of the NMF is nonnegative. The number of components is set to ``50`` but can be adapted.
- **results:**

.. image:: img/misc/config/factorization_nmf.png
    :alt: Two components and the skewness histogram

*Two components and the skewness histogram (open image in new tab for a better view).*

PCA and ICA
-----------

- **file:** ``config_factorization_pca_ica.yaml``
- **context:** performing a PCA and an ICA on the ``.tif`` sequence.
- **specs:** ``element_normalization_method`` is set to ``'z-score'``. The number of components is set to ``50`` but can be adapted.
- **results:**

.. image:: img/misc/config/factorization_pca_ica.png
    :alt: Two components and the skewness histogram

*Two components and the skewness histogram (open image in new tab for a better view).*
