======================================
Welcome to pyAMNESIA's documentation!
======================================

pyAMNESIA is a **py**\ thon pipeline for analysing the **A**\ ctivity and **M**\ orphology of **NE**\ urons using **S**\ keletonization and other **I**\ mage **A**\ nalysis techniques.

.. toctree::
   :maxdepth: 1
   :caption: Getting started

   start_install
   start_howto

.. toctree::
   :maxdepth: 2
   :caption: Overview

   overview_introduction
   overview_skeleton
   overview_clustering
   overview_factorization
   overview_results_time

.. toctree::
   :maxdepth: 2
   :caption: Miscellaneous

   misc_config
   misc_robustness
   misc_support
