=======================
Results and computation
=======================


Results
=======

Here you can find a detailed list of the content of the results folder of each analysis. You may want to read the **OVERVIEW** page that corresponds to the analysis you want to launch before reading this section (see :ref:`Skeleton<skeletonmodule>`, :ref:`Clustering<clusteringmodule>` and :ref:`Factorization<factorizationmodule>`).

Skeleton (:doc:`page <overview_skeleton>`)
    - ``sequence_projection/``

        - ``projected_coordinates.npy``: a file containing the array of components coordinates
        - ``projected_traces.npy``: a file containing the array of components traces
        - ``projected_mask.png``: the binary skeleton (or 'active') mask
        - ``summary_image.png``: the summary_image generated from the movie

    - ``skeleton_mask/``: a folder containing all the intermediate images obtained during :ref:`image skeletonization<im_skel>`:

        - ``0_summary_image.png``
        - ``1_histogram_equalization.png``
        - ``2_smoothing.png``
        - ``3_thresholding.png``
        - ``4_holes_removing.png``
        - ``5_skeletonization.png``
        - ``6_skeleton_cleaning.png``
        - ``7_skeleton_label.png``

Clustering (:doc:`page <overview_clustering>`)
    - ``dimensionality_reduction/``

        - ``pca_res_*.png``: a figure of a trace before and after PCA

    - ``hdbscan_clustering/``

        - ``cluster_*.png``: a figure containing a cluster mask and traces
        - ``hdbscan_*.png``: a scatter plot of traces, projected in a 2D plane

    - ``sequence_projection/``: the same folder than in skeleton, but whose components are the clusters (see :ref:`Consecutive clusterings <consecutive_clusterings>`)


Factorization (:doc:`page <overview_factorization>`)
    - ``skewness/``

        - ``comp_*_to_*.png``: each output component of the factorization, before the skewness selection, with a red or green outline depending on whether the skewness is high enough to be kept (green) or not (red)
        - ``skewness_distribution.png``: histogram distribution of the components' skewnesses

    - ``components/``

        - ``comp_*.png``: each final component of the analysis

-----

Computational performance
=========================
Here are plotted the execution durations of the different modules for some movies. The execution times may vary slightly with the methods and parameters selected. Experiments were conducted on a Intel(R) Core(TM) i7-8565U CPU @1.80GHz.

.. image:: img/overview/results_time/computational_time.png
    :alt: Computational time of the three modules

*Computational time of the three modules.*

.. admonition:: Tip

    Depending on which operation you want to perform on the sequence, you can adjust some parameters (*e.g.* ``crop``) to increase computational speed. You can also cut your movie into smaller ones (*e.g.* :math:`12500 = 5 \times 2500` frames).

    .. image:: img/overview/results_time/crop_diagram.png
        :alt: Cropping process diagram
        :width: 400

    *Cropping process diagram.*
