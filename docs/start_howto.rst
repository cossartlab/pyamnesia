==========
How to use
==========

.. contents:: Table of contents
    :depth: 3
    :local:


pyAMNESIA as a standalone tool
==============================

Use the CLI
-----------

If you want a **lightweight tool**, you may want to use pyAMNESIA **as a standalone tool**, using the **CLI**.

1. *(optional)* Change the ``results_path`` entry of the ``pyamnesia/src/pyamnesia/config.yaml`` file into the absolute path to the directory in which you want the results files to be. By default, results will be stored in the ``pyamnesia/blob/res`` folder.

2. Adapt the parameters in ``pyamnesia/src/pyamnesia/config.yaml`` if you want to. Fore more info on this, please read the **Parameter selection** section of the **OVERVIEW** page that corresponds to the analysis you are running. You can also use the **configuration templates** that are in the ``pyamnesia/src/pyamnesia/config_templates/`` directory, and read the :doc:`Configuration templates <misc_config>` page.

3. Launch the CLI by executing these commands in your terminal:

    .. code:: python

        cd path/to/pyamnesia  # go to project
        cd src
        python pyamnesia.py   # execute CLI

4. The CLI is waiting for your input. Please write ``skeleton``, ``clustering`` or ``factorization`` depending on the analysis you want to run.

5. When the *"Choose input TIF file(s)"* dialog window opens (you may need to search it in your open windows if it does not appear on top of your desktop), select the folder where your data is.

    .. code:: bash

        data/               # folder to select
            example1.tif
            example2.tif
            example3.tif

6. If you launched a ``clustering`` or a ``factorization`` analysis, a *"Choose skeleton results directory"* dialog window opens (see :doc:`here <overview_skeleton>` for more info on the skeleton analysis, and :ref:`here <skip_skel>` for more info on why a prior skeleton analysis is needed). Select the results folder of the corresponding skeleton analysis.

    .. code:: bash

        res/
            Skeleton_2020_01_01.00-00-01/    # folder to select
                example1/
                    branch_validation/
                    sequence_projection/
                    skeleton_mask/
                example2/
                    branch_validation/
                    sequence_projection/
                    skeleton_mask/
            Skeleton_2020_02_02.00-00-02/

    If the skeleton results of a ``.tif`` file cannot be found in the specified folder, the analysis of this file will be skipped. For instance, in the code snippets above, the ``example3.tif`` analysis will be skipped.

Here is a demo of the CLI tool.

.. image:: img/start/howto/demo_cli.gif
    :alt: Demo of the CLI tool

*Demo of the CLI tool.*


Use pyAMNESIA's modules
-----------------------

If you want to **automate the analysis** (schedule the analysis, run them on all the files in a specific directory, add a Python preprocessing step...), you can use pyAMNESIA's modules.

This abstract class is inherited for each of the three modules ``Skeleton``, ``Clustering`` and ``Factorization``:

.. autoclass:: pyamnesia.cli.pyamnesia_analysis.PyamnesiaAnalysis
    :members: run_analysis

In order to use the inherited classes, you have to import them from their respective modules and instanciate them, using

.. code:: python

    from cli.pyamnesia_analysis_name import PyamnesiaAnalysisName
    analysis = PyamnesiaAnalysisName()

where ``name`` is ``'skeleton'``, ``'clustering'`` or ``'factorization'``. For instance, if you want to apply the skeleton analysis to every file in the current working directory:

.. code:: python

    import os
    from cli.pyamnesia_analysis_skeleton import PyamnesiaAnalysisSkeleton
    analysis = PyamnesiaAnalysisSkeleton()

    for file in os.listdir():  # for all files in cwd
        file_path = os.path.abspath(file)
        analysis.run_analysis(file_path)

----------

pyAMNESIA as a CICADA module
============================

If you want to use CICADA_ to run the tool and have a **pretty GUI** to guide you, then you may want to use pyAMNESIA **as a CICADA module**.

.. _CICADA: https://gitlab.com/cossartlab/cicada/-/tree/master/

Setup
-----
You have to add the pyAMNESIA analysis to CICADA. To do so:

1. Launch CICADA:

    .. code:: bash

        cd path/to/cicada  # go to CICADA
        cd src
        python -m cicada   # execute CICADA

2. Add the ``tif_wrapper`` (located in ``pyamnesia/src/pyamnesia/gui/``) to CICADA by clicking on the first ``+``.

3. Add the ``ci_analyses_pyamnesia`` folder (located in ``pyamnesia/src/pyamnesia/gui/``) to CICADA by clicking on the second ``+``.

4. Select the 3 analysis and click ``RUN CICADA``.

.. image:: img/start/howto/demo_cicada_setup.gif
    :alt: Demo of the CICADA setup for using pyAMNESIA

*Demo: CICADA setup for using pyAMNESIA.*

Use
---

1. Launch CICADA:

    .. code:: bash

        cd path/to/cicada  # go to CICADA
        cd src
        python -m cicada   # execute CICADA

2. Select the 3 analysis and click ``RUN CICADA``.

3. Go to ``File > Open new dataset`` or use ``ctrl + O`` and select the folder where your data is.

    .. code:: bash

        data/               # folder to select
            example1.tif
            example2.tif
            example3.tif

4. Select the ``.tif`` files to analyse, then click on the first arrow.

5. Select the analysis to run, then click on the second arrow.

.. image:: img/start/howto/demo_cicada_open.gif
    :alt: Demo: how to select the tif files

*Demo: how to select the* ``.tif`` *files.*

6. If you chose a ``Clustering`` or a ``Factorization`` analysis, select the results folder of the skeleton analysis you run before (see :doc:`here <overview_skeleton>` for more info on the skeleton analysis, and :ref:`here <skip_skel>` for more info on why a prior skeleton analysis is needed).

    .. code:: bash

        res/
            Skeleton_2020_01_01.00-00-01/    # folder to select
                example1/
                    branch_validation/
                    sequence_projection/
                    skeleton_mask/
                example2/
                    branch_validation/
                    sequence_projection/
                    skeleton_mask/
            Skeleton_2020_02_02.00-00-02/

    If the skeleton results of a ``.tif`` file cannot be found in the specified folder, the analysis of this file will be skipped. For instance, in the code snippets above, the ``example3.tif`` analysis will be skipped.

7. Scroll down and choose the results directory.

8. Adapt the parameters if you want to. Fore more info on this, please read the **Parameter selection** section of the **OVERVIEW** page that corresponds to the analysis you are running.

9. Click ``Run analysis``.

.. image:: img/start/howto/demo_cicada_launch.gif
    :alt: Demo: how to launch an analysis

*Demo: how to launch an analysis.*

