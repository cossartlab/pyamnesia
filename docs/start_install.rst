============
Installation
============

We provide two ways to use pyAMNESIA.

- If you want a **lightweight tool** that runs in a command-line interface, you may want to install pyAMNESIA :ref:`as a standalone tool <install-standalone>`;
- If you want to use CICADA_ to run the tool and have a **pretty GUI** to guide you, then you may want to install pyAMNESIA :ref:`as a CICADA module <install-cicada>`.

.. _CICADA: https://gitlab.com/cossartlab/cicada/-/tree/master/

We advise that you use conda_ to create a virtual environment for the tool.

.. code:: bash

    conda create -n pyamnesia python==3.8  # or python==3.7
    conda activate pyamnesia


.. _install-standalone:

Install pyAMNESIA
=================

You only need to ``git clone`` pyAMNESIA:

.. code:: bash

    # clone the repo
    git clone https://gitlab.com/cossartlab/pyamnesia.git
    cd pyamnesia
    # install the dependencies
    sh requirements.sh

.. note:: On Windows, you may not be able to use the ``sh`` command. To solve this, you may either download Cygwin_ or write the commands in ``requirements.sh`` yourself:

    .. code:: bash

        pip install -r requirements.txt
        conda install -c conda-forge skan
        conda install -c conda-forge hdbscan


The installation relies on conda_ to install skan_, which is the package we use to skeletonize images. If you cannot use conda, please install skan_ and hdbscan_ with ``pip``; we cannot guarantee that it will run smoothly though.

.. _Cygwin: http://www.cygwin.com/
.. _conda: https://docs.conda.io/en/latest/
.. _skan: https://jni.github.io/skan/index.html
.. _hdbscan: https://hdbscan.readthedocs.io/

If you want to use pyAMNESIA as a standalone tool, no need for you to read the next section and you can read the :doc:`how-to page <start_howto>` to run the tool.

.. _install-cicada:

Install CICADA
==============

In order to have a GUI to guide you during the pyAMNESIA analysis, you can install CICADA using ``pip``, or from source_ (for more information, please visit `CICADA's documentation`_):

.. code:: bash

    # using pip
    pip install -U pycicada
    # from source
    git clone https://gitlab.com/cossartlab/cicada.git

.. warning::
    As for now, CICADA still is a work in progress, we advise that you clone the stable version we worked on and pip install some additional packages to make it run:

    .. code:: bash

        # install stable version
        git clone -b stable-pyamnesia https://gitlab.com/theodumont/cicada.git
        cd cicada
        # pip install some packages
        pip install -r requirements.txt
        pip install neo elephant

CICADA and pyAMNESIA are now installed and you can go to the :doc:`how-to page <start_howto>` to run the tool.

.. _`CICADA's documentation`: https://pycicada.readthedocs.io/en/latest/install.html
.. _source: https://gitlab.com/cossartlab/cicada

