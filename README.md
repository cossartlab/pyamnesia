# pyAMNESIA

a **py**thon pipeline for analysing the **A**ctivity and **M**orphology of **NE**urons using **S**keletonization and other **I**mage **A**nalysis techniques

**Keywords:** *Clustering, Skeleton, Matrix Factorization, Python, Image Processing, Calcium Imaging*

![INMED logo](docs/img/inmed.png)

## What is pyAMNESIA?

pyAMNESIA is an **image analysis pipeline**. It can perform several general operations, such as **clustering** coactive elements on a video, extracting the **skeleton** of a still structure in a video, of perform PCA, NMF, and other **matrix factorization** techniques.

Documentation and default parameters focus on the application of pyAMNESIA to *Calcium Imaging data*, but you can easily adapt the tool to be relevant on your own data and tasks.

## Documentation

We provide a [documentation](https://pyamnesia.readthedocs.io/) for pyAMNESIA. It includes instructions on how to install it, use it, and adjust its parameters to produce the best result possible.

> :pushpin: If you want a quick and lightweight install to test pyAMNESIA, you can simply run these instructions ([here](https://pyamnesia.readthedocs.io/en/latest/install.html#install-standalone) in the documentation):

```bash
# clone the repo
git clone https://gitlab.com/cossartlab/pyamnesia.git
cd pyamnesia
# install the dependencies
sh requirements.sh
```

